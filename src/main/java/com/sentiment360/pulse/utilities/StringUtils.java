/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sentiment360.pulse.utilities;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Jared
 */
public class StringUtils {
    
    public static final String DEFAULT_DELIM = "<delim>";
    
    public static String joinWithDelim(List<String> strings)
    {
        if (strings == null || strings.isEmpty())
            return "";
        StringBuilder sb = new StringBuilder();
        int size = strings.size();
        for (int i = 0; i < size-1; i++)
            sb.append(strings.get(i)).append(DEFAULT_DELIM);
        sb.append(strings.get(size-1));
        return sb.toString();
    }
    
    public static String joinWithDelim(String[] strings, String delim)
    {
        if (strings == null || strings.length == 0)
            return "";
        StringBuilder sb = new StringBuilder();
        int size = strings.length;
        for (int i = 0; i < size-1; i++)
            sb.append(strings[i]).append(delim);
        sb.append(strings[size-1]);
        return sb.toString();
    }
    
    public static String joinWithDelim(List<String> objects, String delim)
    {
        if (objects == null || objects.isEmpty())
            return "";
        StringBuilder sb = new StringBuilder();
        int size = objects.size();
        for (int i = 0; i < size-1; i++)
            sb.append(objects.get(i)).append(delim);
        sb.append(objects.get(size-1));
        return sb.toString();
    }
    
    public static List<String> splitWithDelim(String string)
    {
        List<String> splitString = new ArrayList<String>();
        if (string == null)
            return splitString;
        
        Collections.addAll(splitString, string.split(DEFAULT_DELIM));
        return splitString;
    }
    
    public static List<String> splitWithDelim(String string, String delim)
    {
        List<String> splitString = new ArrayList<String>();
        if (string == null)
            return splitString;
        
        Collections.addAll(splitString, string.split(delim));
        return splitString;
    }
}
