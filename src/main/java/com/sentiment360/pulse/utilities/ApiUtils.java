/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sentiment360.pulse.utilities;

import com.sentiment360.pulse.core.results.DepartmentResult;
import com.sentiment360.pulse.core.results.UserResult;
import com.sentiment360.pulse.dbclasses.Customer;
import com.sentiment360.pulse.dbclasses.Department;
import com.sentiment360.pulse.dbclasses.DepartmentPermission;
import com.sentiment360.pulse.dbclasses.Ledger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;


/**
 *
 * @author king
 */
@Stateless
public class ApiUtils {

    /** Entity manager which should be passed in from a service **/
    EntityManager _entityManager;
    
    /** A logging object for formatted output to the server log. */
    private Logger _logger;

//    @Resource(mappedName="java:/refreshConnectionFactory")
//    private QueueConnectionFactory _connectionFactory;
    
//    @Resource(mappedName="java:/queue/refreshQueue")
//    private Queue _queue;
    
    //private FoulCompiler _foulChecker; //@TODO: UNCOMMENT ME
    
    private final String MARK_ALL_PROPERTY = "markAllReviews";
    private final String REFRESH_PROPERTY = "refreshCategory";
    private final String RESET_PROPERTY = "resetCategory";
    private final String CATEGORY_PROPERTY = "categoryId";
    private final String MESSAGE_TYPE_PROPERTY = "messageType";
    
    // Number to divide by to turn milliseconds into various time intervals
    private static final long MILLISECONDS_TO_MONTH = 2592000000L;
    private static final long MILLISECONDS_TO_WEEKS = 604800000L;
    private static final long MILLISECONDS_TO_DAYS = 86400000L;
    private static final long MILLISECONDS_TO_HALF_DAYS = 43200000L;
    private static final long MILLISECONDS_TO_HOURS = 3600000L;
    private static final long MILLISECONDS_TO_SECONDS = 1000L;
    private static final String MONTH_INTERVAL = "month";
    private static final String HOUR_INTERVAL = "hour";
    private static final String HALF_DAY_INTERVAL = "half-day";
    private static final String DAY_INTERVAL = "day";
    private static final String WEEK_INTERVAL = "week";
    private static final long EARLIEST_TIMESTAMP = 1356998400L; // Jan. 1 2013 00:00:00 GMT
    
    private static final long PG_VOTE_TIMEOUT = 10;
    
    //private static final Region SQS_REGION = Region.getRegion(Regions.fromName("JENKINSREGIONNAME"));
    //private static final String QUEUE_NAME = "JENKINSQUEUENAME";
    //private SQSConnectionFactory _connectionFactory;
    
    public ApiUtils() {}
    
    public ApiUtils(EntityManager em) {
        _entityManager = em;
    }
    
    @PostConstruct
    public void init(){
        _logger = Logger.getLogger(this.getClass().getName());
        //_foulChecker = FoulCompiler.getInstance();
        //_foulChecker.configure();
        //_apiAccess = APIAccess.getInstance();
        //_connectionFactory = SQSConnectionFactory.builder()
        //        .withRegion(SQS_REGION)
        //        .build();
    }
    
    public EntityManager getEM(){
        return _entityManager;
    }
    
    public void close(){
        _entityManager.close();        
    }
    
    
    public boolean isValidPGNumber(int number) {
        if (number < 1 || number > 4)
            return false;
        
        return true;
    }
    
    public boolean isValidPGQuestion(String question)
    {
        if (question == null)
            return false;
        
        return true;
    }
    
    public boolean isValidDelimOptions(String delim)
    {
        if (delim == null)
            return false;
        
        return true;
    }
    
    public boolean isValidReview(String review)
    {
        if (review == null || review.isEmpty())
            return false;
        
        return true;
    }
    
    public boolean isValidReviewStatus(String status)
    {
        if (status == null || status.isEmpty())
            return false;
        if (status.equalsIgnoreCase("approved"))
            return true;
        if (status.equalsIgnoreCase("pending"))
            return true;
        if (status.equalsIgnoreCase("hidden"))
            return true;
        if (status.equalsIgnoreCase("rejected"))
            return true;
        if (status.equalsIgnoreCase("foul"))
            return true;
        
        return false;
    }
    
    public boolean isValidPGSelection(String selection, Integer categoryId, int pgNumber)
    {
        /*
        if (selection == null || selection.isEmpty())
            return false;
        
        List<String> optionSets = getPowerGaugeOptionSetsHelper(categoryId);
        List<String> options = StringUtils.splitWithDelim(optionSets.get(pgNumber-1));
        
        if (options.contains(selection))
            return true;
        
        return false;
*/ //@TODO: RE-enable
        return true;
    }
    
        /** 
     * Grants the given user access to read data from the given department.
     * @param userId The ID of the user to whom access will be granted.
     * @param departmentId The department to which the user will be granted access.
     * @return Integer - The departmentId if successful.  -1 if the operation fails.
     */ 
    
    public Department getDepartment(Integer departmentId)
    {
        Query q = _entityManager.createQuery("SELECT d FROM Department d WHERE d.departmentId = :departmentId");
        q.setParameter("departmentId", departmentId);
        q.setMaxResults(1);
        Department d = (Department) q.getSingleResult();
        
        return d;
    }
    
    public DepartmentResult compileDepartment(Department dept, boolean applyOtherData) {
        DepartmentResult d = new DepartmentResult();
        
        d.setDepartmentId(dept.getDepartmentId());
        d.setDepartmentName(dept.getDepartmentName());
        Customer owner = dept.getOwner();
        if (owner != null)
        {
            d.setOwnerId(owner.getUserId());
            d.setOwnerLedger(owner.getLedger().getLedgerName());
        }
        else
        {
            d.setOwnerId(-1);
            d.setOwnerLedger("");
        }
        
        if (applyOtherData)
        {
            //applyDepartmentCategoryIds(d); @TODO:reimplement
            //applyDepartmentPermittedUserIds(d);
        }
        else
        {
            d.setCategoryIds(new ArrayList<Integer>());
            d.setPermittedUserIds(new ArrayList<Integer>());
        }
        
        return d;
    }
    
     /* Gets the information for a given user based on their username.
     * @param username The name of the user in question.
     * @return UserResult - Returns the basic information about the given user.
     */
    public UserResult getUserInfo(Integer userId) {
            TypedQuery<Customer> q = _entityManager.createQuery("SELECT u From Customer u WHERE u.userId = :userId", Customer.class);
            q.setParameter("userId", userId);
            Customer user;
            try {
                user = q.getSingleResult();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            return compileUser(user, true);
    }
    
    public List<Integer> getPermittedDepartments(Integer userId)
    {
        Query q = _entityManager.createQuery("SELECT DISTINCT w.department.departmentId FROM DepartmentPermission w WHERE w.user.userId = :userId");
        q.setParameter("userId", userId); 
        List<Integer> ids = (List<Integer>) q.getResultList();
        return ids;
    }
    
    
        public UserResult compileUser(Customer user, boolean applyOtherData) {
        UserResult u = new UserResult();
        
        u.setUserId(user.getUserId());
        u.setUsername(user.getLoginName());
        u.setAccountType(user.getAccountType());
        u.setApiCalls(user.getApiCalls().intValue());
        Ledger ledger = user.getLedger();
        if (ledger != null)
        {
            u.setLedgerName(ledger.getLedgerName());
        }
        else {
            u.setLedgerName("");
        }
        
        if (applyOtherData)
        {
            applyUserWidgetPermissions(u);
        }
        else
        {
            u.setWidgetPermissionsList(new ArrayList<Integer>());
        }
        
        return u;
    }
        
    private UserResult applyUserWidgetPermissions(UserResult u) {
        Query q = _entityManager.createQuery("SELECT DISTINCT w.department.departmentId FROM DepartmentPermission w WHERE w.user.userId=:userId");
        q.setParameter("userId", u.getUserId());
        List<Integer> departmentIds;
        try {
            departmentIds = (List<Integer>) q.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            u.setWidgetPermissionsList(new ArrayList<Integer>());
            return u;
        }
        
        u.setWidgetPermissionsList(departmentIds);
        return u;
    }
    
    private boolean isNonEmptyList(List list)
    {
        if (list == null)
            return false;
        if (list.isEmpty())
            return false;
        return true;
    }
    
            /** 
     * Grants the given user access to read data from the given department.
     * @param userId The ID of the user to whom access will be granted.
     * @param departmentId The department to which the user will be granted access.
     * @return Integer - The departmentId if successful.  -1 if the operation fails.
     */ 
    public Integer addWidgetPermission(Integer userId, Integer departmentId) {
        Customer user = getUser(userId);
        Department department = getDepartment(departmentId);
        
        TypedQuery<DepartmentPermission> checkExistence = getEM().createQuery("SELECT w FROM DepartmentPermission w WHERE w.user.userId=:userId AND w.department.departmentId=:departmentId", DepartmentPermission.class);
        checkExistence.setParameter("userId", userId);
        checkExistence.setParameter("departmentId", departmentId);
        List<DepartmentPermission> exists = checkExistence.getResultList();
        if(exists.size()>0){
            return departmentId;
        }
        System.out.println("User id adding widget permission: "+user.getUserId());

        DepartmentPermission wp = new DepartmentPermission();
        wp.setPermissionId(null);
        wp.setDateCreated(new Date());
        wp.setDepartment(department);
        wp.setUser(user);
        getEM().persist(wp);

        return departmentId;
    }
    
    public Customer getUser(Integer userId){
        Query q = getEM().createQuery("SELECT u From Customer u WHERE u.userId = :userId");
        q.setParameter("userId", userId);
        q.setMaxResults(1);
        Customer u = (Customer) q.getSingleResult();
        return u;
    }

}
