/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sentiment360.pulse.services;

import com.sentiment360.pulse.bidnes.PermissionBiz;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

@Path("/permissions")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class PermissionService {

    @EJB
    PermissionBiz permBiz;
    
    @Context
    private UriInfo uriInfo;
    
    /** A logging object for formatted output to the server log. */
    private Logger _logger = Logger.getLogger(this.getClass().getName());
    
    /** 
     * Grants the given user access to read data from the given department.
     * @param userId The ID of the user to whom access will be granted.
     * @param departmentId The department to which the user will be granted access.
     * @return Integer - The departmentId if successful.  -1 if the operation fails.
     */ 
    public Integer addWidgetPermission(Integer userId, Integer departmentId) throws UnsupportedEncodingException {
        return permBiz.addWidgetPermission(userId,departmentId);
    }
    
    /** 
     * Removes the given user's access to read data from the given department.
     * @param userId The ID of the user whose access will be removed.
     * @param departmentId The department from which access will be removed for the give user.
     * @return boolean - True for success.  False for failure.
     */ 
    public boolean removeWidgetPermission(Integer userId, Integer departmentId) throws UnsupportedEncodingException {
        return permBiz.removeWidgetPermission(userId,departmentId);
    }

    /** 
     * Gets a list of all the deparmentIds to which a user has permission to read data from.
     * @param userId The ID of the user whose department access list is being retrieved.
     * @return List<Integer> - A list of departmentIds.
     */ 
    public List<Integer> getWidgetPermissions(Integer userId) throws UnsupportedEncodingException {
        return permBiz.getWidgetPermissions(userId);
    }
}
