/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sentiment360.pulse.services;

import com.sentiment360.pulse.core.results.DepartmentResult;
import com.sentiment360.pulse.exceptions.InvalidInputException;
import com.sentiment360.pulse.bidnes.DepartmentBiz;
import com.sentiment360.pulse.utilities.InputValidator;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.jboss.resteasy.links.AddLinks;
import org.jboss.resteasy.links.LinkResource;


@Path("/departments")
@Produces({MediaType.APPLICATION_JSON})
//@Consumes({MediaType.APPLICATION_JSON})
public class DepartmentService {
    @EJB
    private DepartmentBiz deptBiz;
    
    private InputValidator inputValidator;
    
    @Context
    private UriInfo uriInfo;
    
    /** A logging object for formatted output to the server log. */
    private Logger _logger = Logger.getLogger(this.getClass().getName());
        
    public DepartmentService() {}
    
    /** 
     *
     * @api {post} /departments/ Create a department
     * @apiName addDepartment
     * @apiGroup Departments
     *
     * @apiParam {String} departmentName New Department's Name.
     * @apiParam {Number} userId Users unique ID.
     *
     * @apiSuccess {Object} response HTTP Response with 201 CREATED status and location header pointing to the new department.
     * 
     * @apiUse UserNotFoundError
     * @apiUse InvalidInputError
     */
    @LinkResource(value = DepartmentResult.class)
    @POST
    @Path("")
    @Consumes({ MediaType.APPLICATION_JSON })
    public JsonObject addDepartment(String input) throws UnsupportedEncodingException {
        
        JsonReader jsonReader = Json.createReader(new StringReader(input));
        JsonObject jsonObject = jsonReader.readObject();
        jsonReader.close();
        
        String departmentName = jsonObject.getString("departmentName");
        Integer userId = jsonObject.getInt("userId");
        
        if (!inputValidator.isValidResourceName(departmentName))
            throw new InvalidInputException("The provided department name is invalid");
        
        Integer deptId=deptBiz.addDepartment(departmentName,userId);
        
        JsonObject response = Json.createObjectBuilder()
        .add("departmentId", deptId)
        .build();
        return response;
    }
    
        
    /**
     * @api {get} /departments/:departmentId Get a department by id.
     * @apiName getDepartment
     * @apiGroup Departments
     *
     * @apiParam {Number} deptId Department ID.
     *
     * @apiSuccess {object} department A department object.
     * @apiSuccessExample {json} Success-Result:
     * {"Department": {
     *    "atom.link":    [
     *             {
     *          "@rel": "remove",
     *          "@href": "http://localhost:8080/octoapi/api/departments/456"
     *       },
     *             {
     *          "@rel": "self",
     *          "@href": "http://localhost:8080/octoapi/api/departments/456"
     *       },
     *             {
     *          "@rel": "list",
     *          "@href": "http://localhost:8080/octoapi/api/departments/456/categories"
     *       },
     *             {
     *          "@rel": "add",
     *          "@href": "http://localhost:8080/octoapi/api/departments"
     *       }
     *    ],
     *    "categoryIds": {"categoryId": 263},
     *    "departmentId": 456,
     *    "departmentName": "Trading"
     * }}
     * 
     * @apiUse DepartmentNotFoundError
     */
    //Accept: application/myapp.2.0.1+json
    @AddLinks
    @LinkResource
//    @RequestMapping(headers="octoapi\\.v1\\+json")
    @GET
    @Path("/{departmentId}")
    public Response getDepartment(@Context Request request, @PathParam("departmentId") Integer departmentId) throws UnsupportedEncodingException {
            
        DepartmentResult foundDepartment = deptBiz.getDepartment(departmentId);
        
        if(foundDepartment == null)
            return Response.status(Response.Status.NOT_FOUND).entity("Department not found!").build();
        
        //ETag header
        EntityTag etag = new EntityTag(Integer.toString(foundDepartment.hashCode()));
        Response.ResponseBuilder builder = request.evaluatePreconditions(etag);

        // cached resource did change -> serve updated content
        if(builder == null) {
            builder = Response.ok(foundDepartment, MediaType.APPLICATION_JSON);
            builder.tag(etag);
        }
        
        
        //Expires header
        Calendar c = Calendar.getInstance(); 
        c.setTime(new Date()); 
        c.add(Calendar.DATE, 1);
        Date date = c.getTime();
        builder.expires(date);
        //CacheControl options header
        CacheControl cc = new CacheControl();
        cc.setMaxAge(300);
//        cc.setPrivate(true);
//        cc.setNoStore(true);
        builder.cacheControl(cc);
        
        

        return builder.build();  
    }
    
    //    [VersionedRoute("api/breachedaccount/{account}", 2)]  
//    [Route("api/v2/breachedaccount/{account}")]
//    @GET
//    @Path("/{departmentId}")
//    public DepartmentResult getDepartmentV2(@Context SecurityContext sec, @PathParam("departmentId") Integer departmentId) throws UnsupportedEncodingException {
//    
////        if (!userHasDepartmentPermission(clientUserName, behalfOf, departmentId))
////            return null;
//        //This block is skipped if not HTTPS/logged in due to sec.isSecure()
//        if (sec.isSecure() && !sec.isUserInRole("ADMIN")) {
//            _logger.log(Level.SEVERE, sec.getUserPrincipal()
//                    + " accessed customer database.");
//        }
//        
//        return _apiUtils.getDepartmentHelper(departmentId);
//    }
    
    /** 
     * Flags the department as deleted to make it inaccessible.
     * @api {delete} /departments/:departmentId Delete a department.
     * @apiName deleteDepartment
     * @apiGroup Departments
     *
     * @apiParam {Number} deptId Department ID.
     *
     * @apiSuccess {Boolean} isSuccessful Status of delete request.
     * 
     * @apiUse DepartmentNotFoundError
     */
    @LinkResource(value = DepartmentResult.class)
    @DELETE
    @Path("/{departmentId}")
    public Boolean deleteDepartment(@PathParam("departmentId") Integer departmentId) throws UnsupportedEncodingException {
        return deptBiz.deleteDepartment(departmentId);
    } 
    
       
//    @GET
//    @Path("")
//    public List<DepartmentResult> getAllDepartments() throws UnsupportedEncodingException {
//        return deptBiz.getAllDepartments();
//    }

}

    