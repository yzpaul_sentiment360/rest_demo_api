/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sentiment360.pulse.services;

import com.sentiment360.pulse.bidnes.UserBiz;
import com.sentiment360.pulse.core.results.DepartmentResult;
import com.sentiment360.pulse.core.results.UserResult;
import com.sentiment360.pulse.utilities.StringUtils;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.jboss.resteasy.links.AddLinks;
import org.jboss.resteasy.links.LinkResource;

@Path("/users")
@Produces({MediaType.APPLICATION_JSON})
public class UserService {
    
    @Context
    private UriInfo uriInfo;
    
    @EJB
    private UserBiz userBiz;
    
    /** A logging object for formatted output to the server log. */
    private Logger _logger = Logger.getLogger(this.getClass().getName());
        
    public UserService() { }

    /**
     * @api {get} /users/:userId Get a user by id
     * @apiName getUser
     * @apiGroup Users
     * 
     * @apiParam {Number} userId User's unique ID.
     * 
     * @apiSuccess {object} user The requested user.
     * @apiSuccessExample {json} Success-Response:
     * {"User": {
     *    "atom.link":    [
     *             {
     *          "@rel": "self",
     *          "@href": "http://localhost:8080/octoapi/api/users/5"
     *       },
     *             {
     *          "@rel": "departments",
     *          "@href": "http://localhost:8080/octoapi/api/users/5/departments"
     *       },
     *             {
     *          "@rel": "categories",
     *          "@href": "http://localhost:8080/octoapi/api/users/5/categories"
     *       }
     *    ],
     *    "accountType": "client",
     *    "apiCalls": 342506,
     *    "DepartmentPermissions": {"departmentId":    [
     *       456,
     *       16252
     *    ]},
     *    "ledgerName": "sentiment360",
     *    "userId": 5,
     *    "username": "flint"
     * }}
     * 
     * @apiUse UserNotFoundError
     */
    /** 
     * Gets the information for a given user based on their username.
     * @param userName The name of the user in question.
     * @return UserResult - Returns the basic information about the given user.
     */
    //TODO(jharrod) Needs to limit how many can be passed in (50? 100? more?)
    @AddLinks
    @LinkResource
    @GET
    @Path("/{userIds}")
    public List<UserResult> getUser(@PathParam("userIds") String userIds) {
        //parse String userIds into a list of strings e.g. "5;46;247" -> ["5","46","247"]
        List<String> userIdsStringList = StringUtils.splitWithDelim(userIds, ",");
        //put list of userId strings into list of userId Integers e.g. [5,46,247]
        List<Integer> userIdsIntegerList = new ArrayList<Integer>();
        for(String userId : userIdsStringList) {
            userIdsIntegerList.add(Integer.valueOf(userId));
        }
        //make a list of UserResults based off passed-in userIds
        List<UserResult> userIdsUserResultList = new ArrayList<UserResult>();
        for(Integer userId : userIdsIntegerList) {
            userIdsUserResultList.add(userBiz.getUserInfo(userId));
        }
        return userIdsUserResultList;
    }  
    
        
    /**
     * @api {get} /users/:userId/departments Get a user's departments
     * @apiName getUserDepartments
     * @apiGroup Users
     *
     * @apiParam {Number} userId User's unique ID.
     *
     * @apiSuccess {object[]} departments A list of the specified user's departments.
     * 
     * @apiUse UserNotFoundError
     */
    @AddLinks
    @LinkResource(value = UserResult.class, rel = "departments")
    @GET
    @Path("/{userId}/departments")
    public List<DepartmentResult> getUserDepartments(@PathParam("userId") Integer userId){
        return userBiz.getUserDepartments(userId);
    }
    
    @AddLinks
    @LinkResource(value = UserResult.class, rel = "users")
    @POST
    @Path("")
    @Consumes({ MediaType.APPLICATION_JSON })    
    public JsonArray addUser(@HeaderParam("Action") String action, String input) {
        //action is string val of either: single or bulk.
        List<JsonObject> jsonObjectList = new ArrayList<JsonObject>();
        switch(action) {
            case "single":
                jsonObjectList.add(addUserHelper(input));
                break;
            case "bulk" :
                JsonReader jsonReader = Json.createReader(new StringReader(input));
                JsonArray jsonArray = jsonReader.readArray();
                int arraySize = jsonArray.size();
                for(int i=0; i<arraySize; i++) {
                    jsonObjectList.add(addUserHelper(jsonArray.getJsonObject(i).toString()));
                }
                break;
            default:
                throw new WebApplicationException("Bad action!");

        }
        JsonReader jsonReader = Json.createReader(new StringReader(jsonObjectList.toString()));
        JsonArray jsonArray = jsonReader.readArray();
        jsonReader.close();
        return jsonArray;    
    }
    
    private JsonObject addUserHelper(String input) {
        JsonReader jsonReader = Json.createReader(new StringReader(input));
        JsonObject jsonObject = jsonReader.readObject();
        jsonReader.close();
        
        String ledgerName = jsonObject.getString("ledgerName");
        String userName = jsonObject.getString("userName");
        String domain = jsonObject.getString("domain");
        String email = jsonObject.getString("email");
        String accountType = jsonObject.getString("accountType");
        
        if (userName == null || userName.isEmpty()) {
            _logger.log(Level.SEVERE, "NavigatorBean.createUser() Invalid user name.");
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
        Integer createdUserId=userBiz.createUser(userName, domain, accountType);
        try {
            JsonObject response = Json.createObjectBuilder()
                .add("userId", createdUserId)
                .build();
            return response;
        } catch(Exception e){
            _logger.log(Level.SEVERE, "NavigatorBean.createUser(): Something went wrong while creating the user entry.");
            _logger.log(Level.SEVERE, e.getMessage());
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    
        //TODO(jharrod) Needs to limit how many can be passed in (50? 100? more?)
    @DELETE
    @Path("/{userIds}")
    public Boolean deleteUser(@PathParam("userIds") Integer userId) throws UnsupportedEncodingException {
        //@TODO:split string to delete one user at a time
        return userBiz.deleteUser(userId);
    }
    
        
    /** 
     * Gets the Id for a given user based on their username.
     * Note: behalfOf argument passed in is -1, as this is meant to return the behalfOf id itself
     * @param userName The name of the user in question.
     * @return Integer - The user's Id.
     */
    @GET
    @Path("/domains/{domain}/usernames/{userName}")
    public Integer getUserId(@PathParam("userName") String userName, @PathParam("domain") String domain) throws UnsupportedEncodingException {
        return userBiz.getUserId(userName,domain);
    }
    
    
    /** 
     * Gets a list of all available users
     * @return List<String> - Returns a list of all users.
     */
    @AddLinks
    @LinkResource
    @GET
    @Path("")
    public List<String> getAllUsers() throws UnsupportedEncodingException {
        return userBiz.getAlllUsers();
    }
    
    
}
