package com.sentiment360.pulse.services;

//http://localhost:8080/QuteeDemoRest-v1/rest/helloservice/json/103

//lsof -i :8080
//kill -9 11154



import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Path("/helloservice")
public class HelloService {
    
    private static Logger _logger;
    
    public HelloService(){
        _logger = Logger.getLogger(HelloService.class.getName());
    }

    private Connection conn() throws SQLException  {
        String host = "jdbc:mysql://dev-backend.cluster-cwtxulgbsb88.us-east-1.rds.amazonaws.com:3306/pulse";//via jdbc and port:db
        String username = "root";
        String password= "YugGndD12";
        try {
                Class.forName("org.mariadb.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                _logger.log(Level.SEVERE, "Unable to find MySQL JDBC driver.");
                return null;
            }
        Connection con = DriverManager.getConnection( host, username, password );
        return (con);
    }

    @GET
    @Path("/json/{p}")
    @Produces({"text/plain"})
    //@Consumes(MediaType.APPLICATION_JSON)
    public String getUserInfo(@PathParam("p") Integer userId){
        try (Connection conn = conn()) {
            String query = "SELECT * FROM pulse.customer WHERE userId = ?";
            PreparedStatement preparedStmt = conn.prepareStatement(query);
            preparedStmt.setInt(1, userId);
            ResultSet rs=preparedStmt.executeQuery();
            if(rs.next())
                return "login name: "+rs.getString("loginName");
            else
                return "shit broke yo";
        }
        catch(SQLException s){
            return "sql exception:"+s.toString();
        }
        catch (NoResultException nre) {
            //throw new UserNotFoundException(userId);
            return "UserNotFoundException";
        } catch (PersistenceException e) {
            //throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
            return "WebApplicationException";
        }
    }
}