/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sentiment360.pulse.core.results;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UserResult implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Integer userId;
    private String username;
    private String accountType;
    private int apiCalls;
    private String ledgerName;
    private List<Integer> widgetPermissionsList;
    
    public UserResult()
    {
        userId = 0;
        username = "";
        accountType = "";
        apiCalls = 0;
        ledgerName = "";
        widgetPermissionsList = new ArrayList<Integer>();
    }
    
    public UserResult(Integer userId, String username, Integer apiCalls, String accountType, String ledgerName){
        this.userId = userId;
        this.username = username;
        this.apiCalls = apiCalls;
        this.accountType = accountType;
        this.ledgerName = ledgerName;
        widgetPermissionsList = new ArrayList<Integer>();
    }
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserResult)) {
            return false;
        }
        UserResult other = (UserResult) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "enkia.pulse.core.User[userId=" + userId + "][userName=" + username + "]";    }


    public int getApiCalls() {
        return apiCalls;
    }

    public void setApiCalls(int apiCalls) {
        this.apiCalls = apiCalls;
    }
    
    public String getAccountType() {
        return accountType;
    }
    
    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }
    
    public List<Integer> getWidgetPermissionsList() {
        return widgetPermissionsList;
    }

    public void setWidgetPermissionsList(List<Integer> widgetPermissionsList) {
        this.widgetPermissionsList = widgetPermissionsList;
    }
    
    public String getLedgerName() {
        return ledgerName;
    }
    
    public void setLedgerName(String ledgerName) {
        this.ledgerName = ledgerName;
    }

}