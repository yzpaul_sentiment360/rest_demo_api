/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sentiment360.pulse.core.results;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DepartmentResult implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Integer departmentId;
    private String departmentName;
    private String ownerLedger;
    private Integer ownerId;
    private List<Integer> permittedUserIds;
    private List<Integer> categoryIds;
    
    
    public DepartmentResult() {
        departmentId = 0;
        departmentName = "";
        permittedUserIds = new ArrayList<Integer>();
        categoryIds = new ArrayList<Integer>();
    }

    public DepartmentResult(Integer departmentId, String departmentName) {
        this.departmentId = departmentId;
        this.departmentName = departmentName;
        permittedUserIds = new ArrayList<Integer>();
        categoryIds = new ArrayList<Integer>();
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }
    
    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
    
    public String getOwnerLedger() {
        return ownerLedger;
    }
    
    public void setOwnerLedger(String ownerLedger) {
        this.ownerLedger = ownerLedger;
    }
    
    public Integer getOwnerId() {
        return ownerId;
    }
    
    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (departmentId != null ? departmentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DepartmentResult)) {
            return false;
        }
        DepartmentResult other = (DepartmentResult) object;
        if ((this.departmentId == null && other.departmentId != null) || 
            (this.departmentId != null && !this.departmentId.equals(other.departmentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "enkia.pulse.core.Department[id=" + departmentId + "]";
    }

    public List<Integer> getPermittedUserIds() {
        return permittedUserIds;
    }

    public void setPermittedUserIds(List<Integer> permittedUserIds) {
        this.permittedUserIds = permittedUserIds;
    }

    public List<Integer> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(List<Integer> categoryIds) {
        this.categoryIds = categoryIds;
    }

}
