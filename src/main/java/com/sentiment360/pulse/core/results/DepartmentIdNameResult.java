/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sentiment360.pulse.core.results;

import java.io.Serializable;

/**
 *
 * @author Jared
 */
public class DepartmentIdNameResult implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private Integer departmentId;
    private String departmentName;

    public DepartmentIdNameResult() {
    }

    public DepartmentIdNameResult(Integer departmentId, String departmentName) {
        this.departmentId = departmentId;
        this.departmentName = departmentName;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }
}
