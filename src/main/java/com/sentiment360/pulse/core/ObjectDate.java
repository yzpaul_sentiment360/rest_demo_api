/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sentiment360.pulse.core;

import java.util.Date;

/**
 *
 * @author Jared
 */
public class ObjectDate {
    private Integer objectId;
    private Date date;
    public ObjectDate(Integer objectId, Date date){
        this.objectId = objectId;
        this.date = date;
    }

    /**
     * @return the objectId
     */
    public Integer getObjectId() {
        return objectId;
    }

    /**
     * @param objectId the objectId to set
     */
    public void setObjectId(Integer ObjectId) {
        this.objectId = ObjectId;
    }

    /**
     * @return the count
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param count the count to set
     */
    public void setDate(Date date) {
        this.date = date;
    }
}
