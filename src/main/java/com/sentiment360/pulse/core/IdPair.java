/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sentiment360.pulse.core;

/**
 *
 * @author Jared
 */
public class IdPair {
    private Integer _primaryId;
    private Integer _secondaryId;
    
    public IdPair(Integer primaryId, Integer secondaryId) {
        _primaryId = primaryId;
        _secondaryId = secondaryId;
    }
    
    public Integer getPrimaryId() {
        return _primaryId;
    }
    
    public void setPrimaryId(Integer primaryId) {
        _primaryId = primaryId;
    }
    
    public Integer getSecondaryId() {
        return _secondaryId;
    }
    
    public void setSecondaryId(Integer secondaryId) {
        _secondaryId = secondaryId;
    }
}
