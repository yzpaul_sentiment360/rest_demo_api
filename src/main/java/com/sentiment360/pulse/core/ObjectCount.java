/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sentiment360.pulse.core;

/**
 *
 * @author fbarrow
 */
public class ObjectCount {

    private Integer objectId;
    private Long count;
    public ObjectCount(Integer objectId, long count){
        this.objectId = objectId;
        this.count = count;
    }

    /**
     * @return the objectId
     */
    public Integer getObjectId() {
        return objectId;
    }

    /**
     * @param objectId the objectId to set
     */
    public void setObjectId(Integer ObjectId) {
        this.objectId = ObjectId;
    }

    /**
     * @return the count
     */
    public Long getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Long count) {
        this.count = count;
    }
}
