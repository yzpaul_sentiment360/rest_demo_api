/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sentiment360.pulse.bidnes;

import com.sentiment360.pulse.core.results.DepartmentResult;
import com.sentiment360.pulse.core.results.UserResult;
import com.sentiment360.pulse.dbclasses.Customer;
import com.sentiment360.pulse.dbclasses.Department;
import com.sentiment360.pulse.dbclasses.Review;
import com.sentiment360.pulse.exceptions.UserNotFoundException;
import com.sentiment360.pulse.utilities.ApiUtils;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

@Stateless
public class UserBiz {
    /** Entity manager to access objects from the database. */
    @PersistenceContext(unitName="PulsePU")
    EntityManager                    _entityManager;
    
    @EJB
    private ApiUtils apiUtils;
    
    public UserBiz(){}
    
    @PostConstruct
    public void init(){
        apiUtils=new ApiUtils(_entityManager);
    }
    
    public UserResult getUserInfo(Integer userId) {
        return apiUtils.getUserInfo(userId);
    }
    
    public Boolean deleteUser(Integer userId) throws UnsupportedEncodingException {        
        TypedQuery<Customer> q1 = _entityManager.createQuery("SELECT u From Customer u WHERE u.userId=:userId", Customer.class);
        q1.setParameter("userId", userId);
        Customer user = q1.getSingleResult();
        
        user.setDeleted(true);
        
        deleteReviewsByUser(userId);
        
        return true;
    }
    
    private void deleteReviewsByUser(Integer userId){
        Query q1 = _entityManager.createQuery("SELECT r FROM Review r WHERE r.author.userId=:userId");
        q1.setParameter("userId", userId);
        
        try {
            List<Review> reviews = q1.getResultList();
            for (Review review : reviews)
            {
                deleteReview(review.getReviewId().intValue());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
    }
    
    private Boolean deleteReview(Integer reviewId) throws UnsupportedEncodingException {
 
        try {
            TypedQuery<Review> q1 = _entityManager.createQuery("SELECT r FROM Review r WHERE r.reviewId=:reviewId", Review.class);
            q1.setParameter("reviewId", reviewId);
            Review review = q1.getSingleResult();
            
            review.setDeleted(true);
            
            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public Integer createUser(String userName, String domain, String accountType) {
        
        if (userName == null || userName.isEmpty())
        {
            //_logger.log(Level.SEVERE, "Invalid user name."); //@TODO: reimplement logging
            return -1;
        }
        
        Customer u;
        try{
            TypedQuery<Customer> q= _entityManager.createQuery("SELECT u From Customer u WHERE LOWER(u.loginName)=LOWER(:userName) AND LOWER(u.domainName)=LOWER(:domainName)", Customer.class);
            q.setParameter("userName", userName);
            q.setParameter("domainName", domain);
            u = q.getSingleResult();
            //_logger.log(Level.SEVERE, "Username already exists in that domain.");
            return -1;
        }catch (NoResultException nre) {
            u=new Customer();
            u.setApiCalls(0L);
            u.setLoginName(userName);
            u.setAccountType(accountType);
            u.setDomainName(domain);
            u.setLedger(null);
            u.setDeleted(false);
            u.setDateCreated(new Date());
            
            //startTransaction();
            _entityManager.persist(u);
            //commitTransaction();

            try{
                return u.getUserId();
            }catch(Exception e){
                e.printStackTrace();
                //_logger.log(Level.SEVERE, "Something went wrong while creating the user entry.");
                return -1;
            }
        }    }

    public List<DepartmentResult> getUserDepartments(Integer userId) {
        UserResult u = apiUtils.getUserInfo(userId);
        if (u == null)
            throw new UserNotFoundException(userId);
        
        List<Integer> deptIds = apiUtils.getPermittedDepartments(u.getUserId());
        
        TypedQuery<Department> q = _entityManager.createQuery("SELECT d FROM Department d WHERE d.deleted=false AND d.departmentId IN :departmentIds ORDER BY LOWER(d.departmentName)", Department.class);
        q.setParameter("departmentIds", deptIds);
        List<Department> results;
        try {
            results = q.getResultList();
        } catch (PersistenceException e) {
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }

        List<DepartmentResult> departments = new ArrayList<DepartmentResult>();
        if (results != null)
        {
            for (Department dept : results)
            {
                DepartmentResult d = apiUtils.compileDepartment(dept, true);
                departments.add(d);
            }
        }
        return departments;    
    }

    public Integer getUserId(String userName, String domain) {
        Query q = _entityManager.createQuery("SELECT u.userId From Customer u WHERE LOWER(u.loginName)=LOWER(:username) AND LOWER(u.domainName) = LOWER(:domain) AND u.deleted=false");
        q.setParameter("username", userName);
        q.setParameter("domain", domain);
        return (Integer) q.getSingleResult();    
    }

    public List<String> getAlllUsers() {
        TypedQuery<Customer> q = _entityManager.createQuery("SELECT u From Customer u WHERE u.deleted=false", Customer.class);
        List<Customer> results;
        List<String> users = new ArrayList();
        try {
            results = q.getResultList();
            for (Customer user : results) {
                users.add(user.getLoginName());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return users;
        }
        return users;    
    }
}
