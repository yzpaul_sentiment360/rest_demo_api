
package com.sentiment360.pulse.bidnes;

import com.sentiment360.pulse.dbclasses.DepartmentPermission;
import com.sentiment360.pulse.utilities.ApiUtils;
import java.io.UnsupportedEncodingException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

public class PermissionBiz {
    /** Entity manager to access objects from the database. */
    @PersistenceContext(unitName="PulsePU")
    EntityManager                    _entityManager;
    
    @EJB
    private ApiUtils apiUtils;
    
    public PermissionBiz(){}
    
    @PostConstruct
    public void init(){
        apiUtils=new ApiUtils(_entityManager);
    }

    public Boolean removeWidgetPermission(Integer userId, Integer departmentId) throws UnsupportedEncodingException {
        TypedQuery<DepartmentPermission> q = _entityManager.createQuery("SELECT w FROM DepartmentPermission w WHERE w.user.userId = :userId AND w.department.departmentId = :departmentId", DepartmentPermission.class);
        q.setParameter("userId", userId);
        q.setParameter("departmentId", departmentId);
        try{
            List<DepartmentPermission> results = q.getResultList();
            for (DepartmentPermission w : results)
            {
                _entityManager.remove(w);
            }
        }catch(Exception e){
            return false;
        } 
        return true;
    }    
    
    
    
    public List<Integer> getWidgetPermissions(Integer userId) throws UnsupportedEncodingException {
        Query q = _entityManager.createQuery("SELECT DISTINCT w.department.departmentId FROM DepartmentPermission w WHERE w.user.userId = :userId");
        q.setParameter("userId", userId);
        List<Integer> wpDeptIds = (List<Integer>) q.getResultList();
        return wpDeptIds;
    }

    public Integer addWidgetPermission(Integer userId, Integer departmentId) {
        return apiUtils.addWidgetPermission(userId, departmentId);
    }
}
