/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sentiment360.pulse.bidnes;

import com.sentiment360.pulse.core.results.DepartmentResult;
import com.sentiment360.pulse.dbclasses.Customer;
import com.sentiment360.pulse.dbclasses.Department;
import com.sentiment360.pulse.utilities.ApiUtils;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author king
 */
@Stateless
public class DepartmentBiz {
    /** Entity manager to access objects from the database. */
    @PersistenceContext(unitName="PulsePU")
    EntityManager                    _entityManager;
    
    @EJB
    private ApiUtils apiUtils;    

    public DepartmentBiz(){}
        
    @PostConstruct
    public void init(){
        apiUtils=new ApiUtils(_entityManager);
    }
        
    public Integer addDepartment(String departmentName, Integer userId) throws UnsupportedEncodingException {
        
        Customer user = apiUtils.getUser(userId);
        
        Department dept = new Department();
        dept.setDepartmentId(null);
        dept.setDepartmentName(departmentName);
        dept.setApiCalls(0);
        dept.setDeleted(false);
        dept.setDateCreated(new Date());
        dept.setOwner(user);
        
        try {
                _entityManager.persist(dept);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
        apiUtils.addWidgetPermission(userId, dept.getDepartmentId());
        //setDefaultStyle(dept); //@TODO: HEY UNCOMMENT ME
        return dept.getDepartmentId();
    }
        
    public DepartmentResult getDepartment(Integer departmentId) {
        
        Department dept = apiUtils.getDepartment(departmentId);
        
        return apiUtils.compileDepartment(dept, true);
    }

    public Boolean deleteDepartment(Integer departmentId) {
        TypedQuery<Department> q = _entityManager.createQuery("SELECT d FROM Department d WHERE d.departmentId = :departmentId'", Department.class);
        q.setParameter("departmentId", departmentId);
        try {
            //startTransaction();
            Department department = q.getSingleResult();
            department.setDeleted(true);
            
            Query q2 = _entityManager.createQuery("SELECT c FROM Category c WHERE c.department.departmentId = :departmentId");
            q2.setParameter("departmentId", departmentId);
//            List<Category> categories = q2.getResultList();
//            for (Category category : categories)
//            {
//                category.setDeleted(true);
//            }
//            commitTransaction();
        } catch (Exception e) {
            return false;
        }

        return true;    
    }
    
//    public List<DepartmentResult> getAllDepartments() throws UnsupportedEncodingException{
        
        //NavigatorBean: public List<DepartmentResult> getAllDepartments-- there is no private version of this method
        
//        List<Integer> deptIds = apiUtils.getPermittedDepartments();
//        
//        List<Department> results = new ArrayList<Department>();
//        if (!deptIds.isEmpty())
//        {
//            TypedQuery<Department> q = _entityManager.createQuery("SELECT d FROM Department d WHERE d.deleted=false AND d.departmentId IN :departmentIds ORDER BY d.departmentName", Department.class);
//            q.setParameter("departmentIds", deptIds);
//            results = q.getResultList();
//        }
//
//        List<DepartmentResult> departments = new ArrayList<DepartmentResult>();
//        if (results != null)
//        {
//            for (Department dept : results)
//            {
//                DepartmentResult d = apiUtils.compileDepartment(dept, true);
//                departments.add(d);
//            }
//        }
//        return departments;
//    }
    
    public List<Integer> getPermittedDepartmentIds(Integer userId)
    {
        Query q = _entityManager.createQuery("SELECT DISTINCT w.department.departmentId FROM DepartmentPermission w WHERE w.user.userId = :userId");
        q.setParameter("userId", userId); 
        List<Integer> ids = (List<Integer>) q.getResultList();
        return ids;
    }
}
