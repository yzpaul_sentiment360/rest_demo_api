/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sentiment360.pulse.exceptions;

/**
 *
 * @author Jared
 */
public class ReviewNotFoundException extends ResourceNotFoundException{
    /**
    * Create a HTTP 404 (Not Found) exception.
    */
    public ReviewNotFoundException()
    {
        super("The review was not found");
    }

    /**
    * Create a HTTP 404 (Not Found) exception.
    * @param message the String that is the entity of the 404 response.
    */
    public ReviewNotFoundException(String message) {
        super(message);
    }
    
    /**
     * @apiDefine ReviewNotFoundError
     * @apiError ReviewNotFound No Review found with the given review id.
     */
    /**
     * Create a HTTP 404 (Not Found) exception.
     * @param reviewId The id of the requested review.
     */
    public ReviewNotFoundException(Integer reviewId) {
        super("No review found with id "+reviewId);
    }
}
