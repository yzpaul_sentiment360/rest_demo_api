/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sentiment360.pulse.exceptions;

/**
 *
 * @author Jared
 */
public class CategoryNotFoundException extends ResourceNotFoundException {
    /**
    * Create a HTTP 404 (Not Found) exception.
    */
    public CategoryNotFoundException()
    {
        super("The category was not found");
    }

    /**
    * Create a HTTP 404 (Not Found) exception.
    * @param message the String that is the entity of the 404 response.
    */
    public CategoryNotFoundException(String message) {
        super(message);
    }
    
    /**
     * @apiDefine CategoryNotFoundError
     * @apiError CategoryNotFound No Category found with the given category id.
     */
    /**
     * Create a HTTP 404 (Not Found) exception.
     * @param categoryId The id of the requested category.
     */
    public CategoryNotFoundException(Integer categoryId) {
        super("No category found with id "+categoryId);
    }
}
