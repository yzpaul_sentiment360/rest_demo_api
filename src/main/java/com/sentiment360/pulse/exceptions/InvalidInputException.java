/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sentiment360.pulse.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

/**
 *
 * @author Jared
 */
public class InvalidInputException extends WebApplicationException {
    
    /**
     * @apiDefine InvalidInputError
     * @apiError InvalidInput Input is not valid.
     */
    public InvalidInputException()
    {
        super(Response.status(Response.Status.BAD_REQUEST).
                entity("Invalid input").type("text/plain").build());
    }
 
    /**
    * Create a HTTP 400 (Bad Request) exception.
    * @param message the String that is the entity of the 400 response.
    */
    public InvalidInputException(String message)
    {
        super(Response.status(Response.Status.BAD_REQUEST).
                entity(message).type("text/plain").build());
    }
}
