/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sentiment360.pulse.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

/**
 *
 * @author Jared
 */
public class InvalidUserException extends WebApplicationException
{
 
    /**
     * @apiDefine InvalidUserError
     * @apiError InvalidUser The user does not have access.
     */
    public InvalidUserException()
    {
        super(Response.status(Response.Status.FORBIDDEN).
                entity("The user does not have access.").type("text/plain").build());
    }
    
    /**
    * Create a HTTP 403 (Forbidden) exception.
    * @param userId the id of the user.
    */
    public InvalidUserException(Integer userId)
    {
        super(Response.status(Response.Status.FORBIDDEN).
                entity("The user with id "+userId+" does not have access.").type("text/plain").build());
    }
 
    /**
    * Create a HTTP 403 (Forbidden) exception.
    * @param message the String that is the entity of the 403 response.
    */
    public InvalidUserException(String message)
    {
        super(Response.status(Response.Status.FORBIDDEN).
                entity(message).type("text/plain").build());
    }
}
