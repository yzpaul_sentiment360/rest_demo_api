/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sentiment360.pulse.exceptions;

/**
 *
 * @author Jared
 */
public class UserNotFoundException extends ResourceNotFoundException{
    /**
    * Create a HTTP 404 (Not Found) exception.
    */
    public UserNotFoundException()
    {
        super("The user was not found");
    }

    /**
    * Create a HTTP 404 (Not Found) exception.
    * @param message the String that is the entity of the 404 response.
    */
    public UserNotFoundException(String message) {
        super(message);
    }
    
    /**
     * @apiDefine UserNotFoundError
     * @apiError UserNotFound No User found with the given user id.
     */
    /**
     * Create a HTTP 404 (Not Found) exception.
     * @param userId The id of the requested user.
     */
    public UserNotFoundException(Integer userId) {
        super("No user found with id "+userId);
    }
}
