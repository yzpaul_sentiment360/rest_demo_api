/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sentiment360.pulse.exceptions;

/**
 *
 * @author Jared
 */
public class SentimentNotFoundException extends ResourceNotFoundException {
    /**
    * Create a HTTP 404 (Not Found) exception.
    */
    public SentimentNotFoundException()
    {
        super("The sentiment was not found");
    }

    /**
    * Create a HTTP 404 (Not Found) exception.
    * @param message the String that is the entity of the 404 response.
    */
    public SentimentNotFoundException(String message) {
        super(message);
    }
    
    /**
     * @apiDefine SentimentNotFoundError
     * @apiError SentimentNotFound No Sentiment found with the given sentiment id.
     */
    /**
     * Create a HTTP 404 (Not Found) exception.
     * @param sentimentId The id of the requested sentiment.
     */
    public SentimentNotFoundException(Integer sentimentId) {
        super("No sentiment found with id "+sentimentId);
    }
}
