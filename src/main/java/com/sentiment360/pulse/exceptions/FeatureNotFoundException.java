/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sentiment360.pulse.exceptions;

/**
 *
 * @author Jared
 */
public class FeatureNotFoundException extends ResourceNotFoundException {
    /**
    * Create a HTTP 404 (Not Found) exception.
    */
    public FeatureNotFoundException()
    {
        super("The feature was not found");
    }

    /**
    * Create a HTTP 404 (Not Found) exception.
    * @param message the String that is the entity of the 404 response.
    */
    public FeatureNotFoundException(String message) {
        super(message);
    }
    
    /**
     * @apiDefine FeatureNotFoundError
     * @apiError FeatureNotFound No Feature found with the given feature id.
     */
    /**
     * Create a HTTP 404 (Not Found) exception.
     * @param featureId The id of the requested feature.
     */
    public FeatureNotFoundException(Integer featureId) {
        super("No feature found with id "+featureId);
    }
}
