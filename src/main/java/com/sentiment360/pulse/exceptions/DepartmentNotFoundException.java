/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sentiment360.pulse.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

/**
 *
 * @author Jared
 */
public class DepartmentNotFoundException extends ResourceNotFoundException{
    /**
    * Create a HTTP 404 (Not Found) exception.
    */
    public DepartmentNotFoundException()
    {
        super("The department was not found");
    }

    /**
    * Create a HTTP 404 (Not Found) exception.
    * @param message the String that is the entity of the 404 response.
    */
    public DepartmentNotFoundException(String message) {
        super(message);
    }
    
    /**
     * @apiDefine DepartmentNotFoundError
     * @apiError DepartmentNotFound No Department found with the given department id.
     */
    /**
     * Create a HTTP 404 (Not Found) exception.
     * @param departmentId The id of the requested department.
     */
    public DepartmentNotFoundException(Integer departmentId) {
        super("No department found with id "+departmentId);
    }
}
