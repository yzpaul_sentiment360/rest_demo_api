/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sentiment360.pulse.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

/**
 *
 * @author Jared
 */
public class ResourceNotFoundException extends WebApplicationException
{
    
    /**
    * Create a HTTP 404 (Not Found) exception.
    */
    public ResourceNotFoundException()
    {
        super(Response.status(Response.Status.NOT_FOUND).
                entity("The resource was not found").type("text/plain").build());
    }

    /**
    * Create a HTTP 404 (Not Found) exception.
    * @param message the String that is the entity of the 404 response.
    */
    public ResourceNotFoundException(String message) {
        super(Response.status(Response.Status.NOT_FOUND).
                entity(message).type("text/plain").build());
    }
}
