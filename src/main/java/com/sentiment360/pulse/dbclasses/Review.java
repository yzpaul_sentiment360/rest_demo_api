/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sentiment360.pulse.dbclasses;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jared
 */
@Entity
@Table(name = "review",
        indexes = {@Index(columnList = "userId"),
                   @Index(columnList = "categoryId"),
                   @Index(columnList = "deleted"),
                   @Index(columnList = "parentId"),
                   @Index(columnList = "status"),
                   @Index(columnList = "isSticky"),
                   @Index(columnList = "starred"),
                   @Index(columnList = "ballot1")})
@XmlRootElement
public class Review implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @TableGenerator(name="REVIEW_GEN", table="SEQUENCE_TABLE", pkColumnName="SEQ_NAME",
            valueColumnName="SEQ_COUNT", pkColumnValue="REVIEW_SEQ")
    @GeneratedValue(strategy = GenerationType.TABLE, generator="REVIEW_GEN")
    @Column(name= "reviewId")
    private Integer reviewId;
    
    @Lob
    @Column(name = "reviewText")
    private String reviewText;
    
    @Lob
    @Column(name = "markedReviewText")
    private String markedReviewText;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dateCreated")
    private Date dateCreated;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dateEdited")
    private Date dateEdited;
    
    @Column(name = "email")
    private String email;
    
    @Column(name = "ipAddress")
    private String ipAddress;
    
    @Column(name = "reviewURL")
    private String reviewURL;
    
    @Column(name = "favorites")
    private int favorites;
    
    @Column(name = "status")
    private String status;
    
    @Column(name = "upvotes")
    private int upvotes;
    
    @Column(name = "downvotes")
    private int downvotes;
    
    @Column(name = "hotness")
    private int hotness;
    
    @Column(name = "ballot1")
    private String ballot1;
    
    @Column(name = "ballot2")
    private String ballot2;
    
    @Column(name = "ballot3")
    private String ballot3;
    
    @Column(name = "ballot4")
    private String ballot4;
    
    @Column(name = "deleted")
    private boolean deleted;
    
    @Lob
    @Column(name = "allReviewSnippets")
    private String allReviewSnippets;
    
    @Column(name= "reviewSnippets")
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "reviewSnippets")
    private List<String> reviewSnippets;
    
    @Column(name = "reported")
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "reported")
    private List<String> reported;
    
    @Column(name = "numTimesReported")
    private int numTimesReported;
    
    @Column(name = "numFeatureMentions")
    private int numFeatureMentions;
    
    @Column(name = "isSticky")
    private boolean isSticky;
    
    @Column(name = "starred")
    private boolean starred;
    
    @Column(name= "uid")
    private String uid;
    
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "categoryId")
    private Category category;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    private Customer author;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parentId")
    private Review parent;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "oldestAncestorId")
    private Review oldestAncestor;
    
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "ancestorId")
    private List<Review> ancestors;
    
    public Review() {}

    public Review(Integer reviewId, String reviewText, String markedReviewText, Date dateCreated, Date dateEdited,
            String email, String ipAddress, String reviewURL, int favorites, String status, int upvotes, int downvotes,
            int hotness, String ballot1, String ballot2, String ballot3, String ballot4, boolean deleted,
            String allReviewSnippets, List<String> reviewSnippets, List<String> reported, int numTimesReported,
            int numFeatureMentions, boolean isSticky, boolean starred, String uid,
            Category category, Customer author, Review parent, Review oldestAncestor, List<Review> ancestors) {
        this.reviewId = reviewId;
        this.reviewText = reviewText;
        this.markedReviewText = markedReviewText;
        this.dateCreated = dateCreated;
        this.dateEdited = dateEdited;
        this.email = email;
        this.ipAddress = ipAddress;
        this.reviewURL = reviewURL;
        this.favorites = favorites;
        this.status = status;
        this.upvotes = upvotes;
        this.downvotes = downvotes;
        this.hotness = hotness;
        this.ballot1 = ballot1;
        this.ballot2 = ballot2;
        this.ballot3 = ballot3;
        this.ballot4 = ballot4;
        this.deleted = deleted;
        this.allReviewSnippets = allReviewSnippets;
        this.reviewSnippets = reviewSnippets;
        this.reported = reported;
        this.numTimesReported = numTimesReported;
        this.numFeatureMentions = numFeatureMentions;
        this.isSticky = isSticky;
        this.starred = starred;
        this.uid = uid;
        this.category = category;
        this.author = author;
        this.parent = parent;
        this.oldestAncestor = oldestAncestor;
        this.ancestors = ancestors;
    }

    public Integer getReviewId() {
        return reviewId;
    }

    public void setReviewId(Integer reviewId) {
        this.reviewId = reviewId;
    }

    public String getReviewText() {
        return reviewText;
    }

    public void setReviewText(String reviewText) {
        this.reviewText = reviewText;
    }

    public String getMarkedReviewText() {
        return markedReviewText;
    }

    public void setMarkedReviewText(String markedReviewText) {
        this.markedReviewText = markedReviewText;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateEdited() {
        return dateEdited;
    }

    public void setDateEdited(Date dateEdited) {
        this.dateEdited = dateEdited;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getReviewURL() {
        return reviewURL;
    }

    public void setReviewURL(String reviewURL) {
        this.reviewURL = reviewURL;
    }

    public int getFavorites() {
        return favorites;
    }

    public void setFavorites(int favorites) {
        this.favorites = favorites;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getUpvotes() {
        return upvotes;
    }

    public void setUpvotes(int upvotes) {
        this.upvotes = upvotes;
    }

    public int getDownvotes() {
        return downvotes;
    }

    public void setDownvotes(int downvotes) {
        this.downvotes = downvotes;
    }

    public int getHotness() {
        return hotness;
    }

    public void setHotness(int hotness) {
        this.hotness = hotness;
    }

    public String getBallot1() {
        return ballot1;
    }

    public void setBallot1(String ballot1) {
        this.ballot1 = ballot1;
    }

    public String getBallot2() {
        return ballot2;
    }

    public void setBallot2(String ballot2) {
        this.ballot2 = ballot2;
    }

    public String getBallot3() {
        return ballot3;
    }

    public void setBallot3(String ballot3) {
        this.ballot3 = ballot3;
    }

    public String getBallot4() {
        return ballot4;
    }

    public void setBallot4(String ballot4) {
        this.ballot4 = ballot4;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getAllReviewSnippets() {
        return allReviewSnippets;
    }

    public void setAllReviewSnippets(String allReviewSnippets) {
        this.allReviewSnippets = allReviewSnippets;
    }

    public List<String> getReviewSnippets() {
        return reviewSnippets;
    }

    public void setReviewSnippets(List<String> reviewSnippets) {
        this.reviewSnippets = reviewSnippets;
    }

    public List<String> getReported() {
        return reported;
    }

    public void setReported(List<String> reported) {
        this.reported = reported;
    }

    public int getNumTimesReported() {
        return numTimesReported;
    }

    public void setNumTimesReported(int numTimesReported) {
        this.numTimesReported = numTimesReported;
    }

    public int getNumFeatureMentions() {
        return numFeatureMentions;
    }

    public void setNumFeatureMentions(int numFeatureMentions) {
        this.numFeatureMentions = numFeatureMentions;
    }

    public boolean isSticky() {
        return isSticky;
    }

    public void setSticky(boolean isSticky) {
        this.isSticky = isSticky;
    }

    public boolean isStarred() {
        return starred;
    }

    public void setStarred(boolean starred) {
        this.starred = starred;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Customer getAuthor() {
        return author;
    }

    public void setAuthor(Customer author) {
        this.author = author;
    }

    public Review getParent() {
        return parent;
    }

    public void setParent(Review parent) {
        this.parent = parent;
    }

    public Review getOldestAncestor() {
        return oldestAncestor;
    }

    public void setOldestAncestor(Review oldestAncestor) {
        this.oldestAncestor = oldestAncestor;
    }

    public List<Review> getAncestors() {
        return ancestors;
    }

    public void setAncestors(List<Review> ancestors) {
        this.ancestors = ancestors;
    }
    
    public String getBallot(Integer ballotIndex) {
        switch(ballotIndex){
            case 1:
                return getBallot1();
            case 2:
                return getBallot2();
            case 3:
                return getBallot3();
            case 4:
                return getBallot4();
        }
        return "";
    }
    
    public void setBallot(Integer position, String ballot) {
        switch(position){
            case 1:
                this.setBallot1(ballot);
                break;
            case 2:
                this.setBallot2(ballot);
                break;
            case 3:
                this.setBallot3(ballot);
                break;
            case 4:
                this.setBallot4(ballot);
                break;
        }
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.reviewId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Review other = (Review) obj;
        if (!Objects.equals(this.reviewId, other.reviewId)) {
            return false;
        }
        return true;
    }
}
