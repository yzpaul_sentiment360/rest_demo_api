package com.sentiment360.pulse.dbclasses;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
/**
 *
 * @author Ali Baba
 */
@Entity
@Table(name = "ledger",
        indexes = {@Index(columnList = "ledgerName")})
@XmlRootElement
public class Ledger implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @TableGenerator(name="LEDGER_GEN", table="SEQUENCE_TABLE", pkColumnName="SEQ_NAME",
            valueColumnName="SEQ_COUNT", pkColumnValue="LEDGER_SEQ")
    @GeneratedValue(strategy = GenerationType.TABLE, generator="LEDGER_GEN")
    @Column(name= "ledgerId")
    private Integer ledgerId;
    
    @Basic(optional = false)
    @Column(name="ledgerName", unique=true)
    private String ledgerName;
    
    @Basic(optional = false)
    @Column(name="apiKey")
    private String apiKey;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Basic(optional = false)
    @Column(name="dateCreated")
    private Date dateCreated;
    
    @Column(name="currentIP")
    private String currentIP; 
    
    @Column(name="userEndPoint")
    private String userEndPoint;
    
    @Column(name="apiCalls")
    private Long apiCalls;
    
    @Column(name = "company")
    private String company;
    
    @Column(name = "deleted")
    private boolean deleted;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    private Customer ledgerOwner;
    
    public Ledger() {}

    public Ledger(Integer ledgerId, String ledgerName, String apiKey, Date dateCreated, String currentIP, String userEndPoint, Long apiCalls, String company, boolean deleted) {
        this.ledgerId = ledgerId;
        this.ledgerName = ledgerName;
        this.apiKey = apiKey;
        this.dateCreated = dateCreated;
        this.currentIP = currentIP;
        this.userEndPoint = userEndPoint;
        this.apiCalls = apiCalls;
        this.company = company;
        this.deleted = deleted;
    }

    public Integer getLedgerId() {
        return ledgerId;
    }

    public void setLedgerId(Integer ledgerId) {
        this.ledgerId = ledgerId;
    }

    public String getLedgerName() {
        return ledgerName;
    }

    public void setLedgerName(String ledgerName) {
        this.ledgerName = ledgerName;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getCurrentIP() {
        return currentIP;
    }

    public void setCurrentIP(String currentIP) {
        this.currentIP = currentIP;
    }

    public String getUserEndPoint() {
        return userEndPoint;
    }

    public void setUserEndPoint(String userEndPoint) {
        this.userEndPoint = userEndPoint;
    }

    public Long getApiCalls() {
        return apiCalls;
    }

    public void setApiCalls(Long apiCalls) {
        this.apiCalls = apiCalls;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Customer getLedgerOwner() {
        return ledgerOwner;
    }

    public void setLedgerOwner(Customer owner) {
        this.ledgerOwner = owner;
    }
}
