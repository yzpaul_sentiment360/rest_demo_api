/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sentiment360.pulse.dbclasses;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jared
 */
@Entity
@Table(name = "ballot",
        indexes = {@Index(columnList = "categoryId"),
                   @Index(columnList = "userId"),
                   @Index(columnList = "deleted"),
                   @Index(columnList = "ballot1")})
@XmlRootElement
public class Ballot implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @TableGenerator(name="BALLOT_GEN", table="SEQUENCE_TABLE", pkColumnName="SEQ_NAME",
            valueColumnName="SEQ_COUNT", pkColumnValue="BALLOT_SEQ")
    @GeneratedValue(strategy = GenerationType.TABLE, generator="BALLOT_GEN")
    @Column(name= "ballotId")
    private Integer ballotId;
    
    @Column(name = "ballot1")
    private String ballot1;
    
    @Column(name = "ballot2")
    private String ballot2;
    
    @Column(name = "ballot3")
    private String ballot3;
    
    @Column(name = "ballot4")
    private String ballot4;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dateCreated")
    private Date dateCreated;
    
    @Column(name = "deleted")
    private boolean deleted;
    
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "categoryId")
    private Category category;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    private Customer author;
    
    public Ballot() {}

    public Ballot(Integer ballotId, String ballot1, String ballot2, String ballot3, String ballot4,
            Date dateCreated, boolean deleted, Category category, Customer author) {
        this.ballotId = ballotId;
        this.ballot1 = ballot1;
        this.ballot2 = ballot2;
        this.ballot3 = ballot3;
        this.ballot4 = ballot4;
        this.dateCreated = dateCreated;
        this.deleted = deleted;
        this.category = category;
        this.author = author;
    }

    public Integer getBallotId() {
        return ballotId;
    }

    public void setBallotId(Integer ballotId) {
        this.ballotId = ballotId;
    }

    public String getBallot1() {
        return ballot1;
    }

    public void setBallot1(String ballot1) {
        this.ballot1 = ballot1;
    }

    public String getBallot2() {
        return ballot2;
    }

    public void setBallot2(String ballot2) {
        this.ballot2 = ballot2;
    }

    public String getBallot3() {
        return ballot3;
    }

    public void setBallot3(String ballot3) {
        this.ballot3 = ballot3;
    }

    public String getBallot4() {
        return ballot4;
    }

    public void setBallot4(String ballot4) {
        this.ballot4 = ballot4;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Customer getAuthor() {
        return author;
    }

    public void setAuthor(Customer author) {
        this.author = author;
    }
    
    public String getBallot(Integer ballotIndex) {
        switch(ballotIndex){
            case 1:
                return getBallot1();
            case 2:
                return getBallot2();
            case 3:
                return getBallot3();
            case 4:
                return getBallot4();
        }
        return "";
    }
    
    public void setBallot(Integer position, String ballot) {
        switch(position){
            case 1:
                this.setBallot1(ballot);
                break;
            case 2:
                this.setBallot2(ballot);
                break;
            case 3:
                this.setBallot3(ballot);
                break;
            case 4:
                this.setBallot4(ballot);
                break;
        }
    }
}
