/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sentiment360.pulse.dbclasses;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jared
 */
@Entity
@Table(name = "departmentPermission",
        indexes = {@Index(columnList = "departmentId"),
                   @Index(columnList = "userId")})
@XmlRootElement
public class DepartmentPermission implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @TableGenerator(name="DEPARTMENTPERMISSION_GEN", table="SEQUENCE_TABLE", pkColumnName="SEQ_NAME",
            valueColumnName="SEQ_COUNT", pkColumnValue="DEPARTMENTPERMISSION_SEQ")
    @GeneratedValue(strategy = GenerationType.TABLE, generator="DEPARTMENTPERMISSION_GEN")
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "permissionId")
    private Integer permissionId;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dateCreated")
    private Date dateCreated;
    
    @ManyToOne(optional = false)
    @JoinColumn(name = "departmentId")
    private Department department;
    
    @ManyToOne(optional = false)
    @JoinColumn(name = "userId")
    private Customer user;
    
    public DepartmentPermission() {}

    public DepartmentPermission(Integer permissionId, Date dateCreated, Department department, Customer user) {
        this.permissionId = permissionId;
        this.dateCreated = dateCreated;
        this.department = department;
        this.user = user;
    }

    public Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Customer getUser() {
        return user;
    }

    public void setUser(Customer user) {
        this.user = user;
    }
}
