/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sentiment360.pulse.dbclasses;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jared
 */
@Entity
@Table(name = "department",
        indexes = {@Index(columnList = "deleted"),
                   @Index(columnList = "userId")})
@XmlRootElement
public class Department implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @TableGenerator(name="DEPARTMENT_GEN", table="SEQUENCE_TABLE", pkColumnName="SEQ_NAME",
            valueColumnName="SEQ_COUNT", pkColumnValue="DEPARTMENT_SEQ")
    @GeneratedValue(strategy = GenerationType.TABLE, generator="DEPARTMENT_GEN")
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "departmentId")
    private Integer departmentId;
    
    @Column(name = "departmentName", length = 300)
    private String departmentName;
    
    @Column(name = "apiCalls")
    private int apiCalls;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dateCreated")
    private Date dateCreated;
    
    @Column(name = "deleted")
    private boolean deleted;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    private Customer owner;

    public Department() {}

    public Department(Integer departmentId, String departmentName, int apiCalls, Date dateCreated, boolean deleted, Customer owner) {
        this.departmentId = departmentId;
        this.departmentName = departmentName;
        this.apiCalls = apiCalls;
        this.dateCreated = dateCreated;
        this.deleted = deleted;
        this.owner = owner;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public int getApiCalls() {
        return apiCalls;
    }

    public void setApiCalls(int apiCalls) {
        this.apiCalls = apiCalls;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Customer getOwner() {
        return owner;
    }

    public void setOwner(Customer owner) {
        this.owner = owner;
    }
}
