/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sentiment360.pulse.dbclasses;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jared
 */
@Entity
@Table(name = "customer",
        indexes = {@Index(columnList = "loginName"),
                   @Index(columnList = "domainName"),
                   @Index(columnList = "ledgerId")})
@XmlRootElement
public class Customer implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @TableGenerator(name="CUSTOMER_GEN", table="SEQUENCE_TABLE", pkColumnName="SEQ_NAME",
            valueColumnName="SEQ_COUNT", pkColumnValue="CUSTOMER_SEQ")
    @GeneratedValue(strategy = GenerationType.TABLE, generator="CUSTOMER_GEN")
    @Column(name= "userId")
    private Integer userId;
    
    @Column(name = "loginName")
    private String loginName;
    
    @Column(name = "domainName")
    private String domainName;
    
    @Column(name="accountType")
    private String accountType;
    
    @Column(name = "apiCalls")
    private Long apiCalls;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dateCreated")
    private Date dateCreated;
    
    @Column(name = "deleted")
    private boolean deleted;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ledgerId")
    private Ledger ledger;

    public Customer() {}

    public Customer(Integer userId, String loginName, String domainName, String accountType, Long apiCalls, Date dateCreated, boolean deleted, Ledger ledger) {
        this.userId = userId;
        this.loginName = loginName;
        this.domainName = domainName;
        this.accountType = accountType;
        this.apiCalls = apiCalls;
        this.dateCreated = dateCreated;
        this.deleted = deleted;
        this.ledger = ledger;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public Long getApiCalls() {
        return apiCalls;
    }

    public void setApiCalls(Long apiCalls) {
        this.apiCalls = apiCalls;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Ledger getLedger() {
        return ledger;
    }

    public void setLedger(Ledger ledger) {
        this.ledger = ledger;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.userId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Customer other = (Customer) obj;
        if (!Objects.equals(this.userId, other.userId)) {
            return false;
        }
        return true;
    }
}
