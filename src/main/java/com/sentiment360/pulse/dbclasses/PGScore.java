/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sentiment360.pulse.dbclasses;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jared
 */
@Entity
@Table(name = "pgScore",
        indexes = {@Index(columnList = "categoryId")})
@XmlRootElement
public class PGScore implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @TableGenerator(name="PGSCORE_GEN", table="SEQUENCE_TABLE", pkColumnName="SEQ_NAME",
            valueColumnName="SEQ_COUNT", pkColumnValue="PGSCORE_SEQ")
    @GeneratedValue(strategy = GenerationType.TABLE, generator="PGSCORE_GEN")
    @Column(name= "scoreId")
    private Integer scoreId;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dateCreated")
    private Date dateCreated;
    
    @Column(name = "powerSentimentScore")
    private float powerSentiment;
    
    @Column(name = "generalSentimentScore")
    private float generalSentimentScore;
    
    @Column(name = "numberOfVotes")
    private int numberOfVotes;
    
    @Column(name = "standardDeviation")
    private double standardDeviation;
    
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "categoryId")
    private Category category;
    
    public PGScore() {}

    public PGScore(Integer scoreId, Date dateCreated, float powerSentiment, float generalSentimentScore, int numberOfVotes, double standardDeviation, Category category) {
        this.scoreId = scoreId;
        this.dateCreated = dateCreated;
        this.powerSentiment = powerSentiment;
        this.generalSentimentScore = generalSentimentScore;
        this.numberOfVotes = numberOfVotes;
        this.standardDeviation = standardDeviation;
        this.category = category;
    }
    
    public PGScore(Date dateCreated, float powerSentiment, float generalSentimentScore, int numberOfVotes, double standardDeviation, Category category) {
        this.dateCreated = dateCreated;
        this.powerSentiment = powerSentiment;
        this.generalSentimentScore = generalSentimentScore;
        this.numberOfVotes = numberOfVotes;
        this.standardDeviation = standardDeviation;
        this.category = category;
    }

    public Integer getScoreId() {
        return scoreId;
    }

    public void setScoreId(Integer scoreId) {
        this.scoreId = scoreId;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public float getPowerSentiment() {
        return powerSentiment;
    }

    public void setPowerSentiment(float powerSentiment) {
        this.powerSentiment = powerSentiment;
    }

    public float getGeneralSentimentScore() {
        return generalSentimentScore;
    }

    public void setGeneralSentimentScore(float generalSentimentScore) {
        this.generalSentimentScore = generalSentimentScore;
    }

    public int getNumberOfVotes() {
        return numberOfVotes;
    }

    public void setNumberOfVotes(int numberOfVotes) {
        this.numberOfVotes = numberOfVotes;
    }

    public double getStandardDeviation() {
        return standardDeviation;
    }

    public void setStandardDeviation(double standardDeviation) {
        this.standardDeviation = standardDeviation;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.scoreId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PGScore other = (PGScore) obj;
        if (!Objects.equals(this.scoreId, other.scoreId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "s360.core.PowerGaugeScore[ id=" + scoreId + " ]";
    }
}
