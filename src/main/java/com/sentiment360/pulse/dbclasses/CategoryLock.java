/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sentiment360.pulse.dbclasses;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jared
 */
@Entity
@Table(name = "categoryLock")
@XmlRootElement
public class CategoryLock implements Serializable {
//    private static final long serialVersionUID = 1L;
    
    @Id
    @TableGenerator(name="CATEGORYLOCK_GEN", table="SEQUENCE_TABLE", pkColumnName="SEQ_NAME",
            valueColumnName="SEQ_COUNT", pkColumnValue="CATEGORYLOCK_SEQ")
    @GeneratedValue(strategy = GenerationType.TABLE, generator="CATEGORYLOCK_GEN")
    @Basic(optional = false)
    @Column(name = "lockId")
    private Integer lockId;
    
    @Basic(optional = false)
    @Column(name = "categoryId", unique=true)
    private Integer categoryId;

    public CategoryLock() {
    }

    public CategoryLock(Integer categoryId) {
        this.categoryId = categoryId;
    }
    
    public Integer getLockId() {
        return lockId;
    }

    public void setLockId(Integer lockId) {
        this.lockId = lockId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }
}
