/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sentiment360.pulse.dbclasses;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jared
 */
@Entity
@Table(name = "sfp",
        indexes = {@Index(columnList = "reviewId"),
                   @Index(columnList = "categoryId"),
                   @Index(columnList = "featureId"),
                   @Index(columnList = "sentimentId")})
@XmlRootElement
public class SFP implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @TableGenerator(name="SFP_GEN", table="SEQUENCE_TABLE", pkColumnName="SEQ_NAME",
            valueColumnName="SEQ_COUNT", pkColumnValue="SFP_SEQ", allocationSize = 500)
    @GeneratedValue(strategy = GenerationType.TABLE, generator="SFP_GEN")
    @Column(name= "sfpId")
    private Long sfpId;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dateCreated")
    private Date dateCreated;
    
    @Lob
    @Column(name = "allReviewSnippets")
    private String allReviewSnippets;
    
    @Lob
    @Column(name= "reviewSnippets")
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "sfpSnippets")
    private List<String> reviewSnippets;
    
    @Lob
    @Column(name = "markedReviewText")
    private String markedReviewText;
    
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "categoryId")
    private Category category;
    
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "reviewId")
    private Review review;
    
    @ManyToOne(optional = false)
    @JoinColumn(name = "featureId")
    private Feature feature;
    
    @ManyToOne
    @JoinColumn(name = "sentimentId")
    private Sentiment sentiment;
    
    public SFP() {}

    public SFP(Long sfpId, Date dateCreated, String allReviewSnippets, List<String> reviewSnippets, String markedReviewText,
            Category category, Review review, Feature feature, Sentiment sentiment) {
        this.sfpId = sfpId;
        this.dateCreated = dateCreated;
        this.allReviewSnippets = allReviewSnippets;
        this.reviewSnippets = reviewSnippets;
        this.markedReviewText = markedReviewText;
        this.category = category;
        this.review = review;
        this.feature = feature;
        this.sentiment = sentiment;
    }

    public Long getSfpId() {
        return sfpId;
    }

    public void setSfpId(Long sfpId) {
        this.sfpId = sfpId;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getAllReviewSnippets() {
        return allReviewSnippets;
    }

    public void setAllReviewSnippets(String allReviewSnippets) {
        this.allReviewSnippets = allReviewSnippets;
    }

    public List<String> getReviewSnippets() {
        return reviewSnippets;
    }

    public void setReviewSnippets(List<String> reviewSnippets) {
        this.reviewSnippets = reviewSnippets;
    }

    public String getMarkedReviewText() {
        return markedReviewText;
    }

    public void setMarkedReviewText(String markedReviewText) {
        this.markedReviewText = markedReviewText;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Review getReview() {
        return review;
    }

    public void setReview(Review review) {
        this.review = review;
    }

    public Feature getFeature() {
        return feature;
    }

    public void setFeature(Feature feature) {
        this.feature = feature;
    }

    public Sentiment getSentiment() {
        return sentiment;
    }

    public void setSentiment(Sentiment sentiment) {
        this.sentiment = sentiment;
    }
}
