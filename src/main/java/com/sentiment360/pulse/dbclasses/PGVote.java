/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sentiment360.pulse.dbclasses;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jared
 */
@Entity
@Table(name = "pgVote",
        indexes = {@Index(columnList = "categoryId"),
                   @Index(columnList = "userId")})
@XmlRootElement
public class PGVote implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @TableGenerator(name="PGVOTE_GEN", table="SEQUENCE_TABLE", pkColumnName="SEQ_NAME",
            valueColumnName="SEQ_COUNT", pkColumnValue="PGVOTE_SEQ")
    @GeneratedValue(strategy = GenerationType.TABLE, generator="PGVOTE_GEN")
    @Column(name= "voteId")
    private Integer voteId;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dateCreated")
    private Date dateCreated;
    
    @Column(name = "vote")
    private float vote;
    
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    private Customer user;
    
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "categoryId")
    private Category category;
    
    public PGVote() { }

    public PGVote(Integer voteId, Date dateCreated, float vote, Customer user, Category category) {
        this.voteId = voteId;
        this.dateCreated = dateCreated;
        this.vote = vote;
        this.user = user;
        this.category = category;
    }
    
    public PGVote(Date dateCreated, float vote, Customer user, Category category) {
        this.dateCreated = dateCreated;
        this.vote = vote;
        this.user = user;
        this.category = category;
    }

    public Integer getVoteId() {
        return voteId;
    }

    public void setVoteId(Integer voteId) {
        this.voteId = voteId;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public float getVote() {
        return vote;
    }

    public void setVote(float vote) {
        this.vote = vote;
    }

    public Customer getUser() {
        return user;
    }

    public void setUser(Customer user) {
        this.user = user;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.user);
        hash = 59 * hash + Objects.hashCode(this.category);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PGVote other = (PGVote) obj;
        if (!Objects.equals(this.user, other.user)) {
            return false;
        }
        if (!Objects.equals(this.category, other.category)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "s360.core.PowerGaugeVote[ id=" + voteId + " ]";
    }
}
