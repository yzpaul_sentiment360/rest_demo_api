/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sentiment360.pulse.dbclasses;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jared
 */
@Entity
@Table(name = "sentiment",
        indexes = {@Index(columnList = "categoryId"),
                   @Index(columnList = "starred")})
@XmlRootElement
public class Sentiment implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @TableGenerator(name="SENTIMENT_GEN", table="SEQUENCE_TABLE", pkColumnName="SEQ_NAME",
            valueColumnName="SEQ_COUNT", pkColumnValue="SENTIMENT_SEQ")
    @GeneratedValue(strategy = GenerationType.TABLE, generator="SENTIMENT_GEN")
    @Column(name= "sentimentId")
    private Integer sentimentId;
    
    @Column(name = "sentimentName")
    private String sentimentName;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dateCreated")
    private Date dateCreated;
    
    @ManyToMany
    @JoinColumn(name = "reviewId")
    private List<Review> reviews;
    
    @Column(name = "cluster")
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "sentimentCluster")
    private List<String> cluster;
    
    @Column(name = "starred")
    private boolean starred;
    
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "categoryId")
    private Category category;
    
    public Sentiment() {}

    public Sentiment(Integer sentimentId, String sentimentName, Date dateCreated,
            List<Review> reviews, List<String> cluster, boolean starred, Category category) {
        this.sentimentId = sentimentId;
        this.sentimentName = sentimentName;
        this.dateCreated = dateCreated;
        this.reviews = reviews;
        this.cluster = cluster;
        this.starred = starred;
        this.category = category;
    }

    public Integer getSentimentId() {
        return sentimentId;
    }

    public void setSentimentId(Integer sentimentId) {
        this.sentimentId = sentimentId;
    }

    public String getSentimentName() {
        return sentimentName;
    }

    public void setSentimentName(String sentimentName) {
        this.sentimentName = sentimentName;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public List<String> getCluster() {
        return cluster;
    }

    public void setCluster(List<String> cluster) {
        this.cluster = cluster;
    }
    
    public boolean isStarred() {
        return starred;
    }

    public void setStarred(boolean starred) {
        this.starred = starred;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
