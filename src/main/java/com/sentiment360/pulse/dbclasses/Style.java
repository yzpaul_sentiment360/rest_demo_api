
package com.sentiment360.pulse.dbclasses;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
/**
 * The entity class for a style object.
 * Contains the style settings for a portal.
 * @author Ali Baba
 *
 */
@Entity
@Table(name = "style",
        indexes = {@Index(columnList = "departmentId")})
public class Style implements Serializable {
    private static final long serialVersionUID = 1L;
    
    /**
     * The primary key.
     */
    @Id
    @TableGenerator(name="STYLE_GEN", table="SEQUENCE_TABLE", pkColumnName="SEQ_NAME",
            valueColumnName="SEQ_COUNT", pkColumnValue="STYLE_SEQ")
    @GeneratedValue(strategy = GenerationType.TABLE, generator="STYLE_GEN")
    @Column(name= "styleId")
    private Integer styleId;
    
    /**
     * The size/shape of the portal.
     */
    @Column(name="widgetSize")
    private String widgetSize;
    
    /**
     * The size/shape of the portal.
     */
    @Column(name="description")
    private String description;
    
    /**
     * Determines whether the background is a default or a custom image.
     */
    @Column(name="backGroundImageType")
    private String backgroundImageType;
    
    /**
     * If the background is set to custom, the path of the image.
     */
    @Column(name="imagePath")
    private String imagePath;
    /**
     * Custom CSS settings for a portal.
     */
    @Lob
    @Column(name="css")
    private String css;
    /**
     * Determines whether the tag cloud is animated or  not.
     *TODO (@Tommy) finish implementation fo this in the widget project.
     */
    @Column(name="animated")
    private boolean animated;
    /**
     * The path to custom, branding, logo images.
     */
    @Column(name="brandingPath")
    private String brandingPath;
    /**
     * If true, the widget displays the octopii logo.
     */
    @Column(name="sentiment360Branding")
    private boolean sentiment360Branding;
    /**
     * The color of the tags in the tag cloud.
     */
    @Column(name="tagColor")
    private String tagColor;
    /**
     * The color of the outer tags on the after a tag is selected.
     */
    @Column(name="fadedTagColor")
    private String fadedTagColor;
    /**
     * The color of the question at the top of the portal.
     */
    @Column(name="questionColor")
    private String questionColor;
    /**
     * The color of the text in in the comments/ tab section of the portal.
     */
    @Column(name="textColor")
    private String textColor;
    /**
     * The color of the graph.
     */
    @Column(name="graphColor")
    private String graphColor;
    /**
     * The color of the scroll bar thumb.
     */
    @Column(name="scrollBarColor")
    private String scrollBarColor;
    /**
     * The background color of the portal (only visible if there is no background image)
     */
    @Column(name="backGroundColor")
    private String backGroundColor;
    
    /**
     * The department (portal/widget) that the style settings apply to.
     */
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "departmentId")
    private Department department;

    public Style(){};
    
    public Style(Style other)
    {
        this.animated = other.animated;
        this.backGroundColor = other.backGroundColor;
        this.backgroundImageType = other.backgroundImageType;
        this.brandingPath = other.brandingPath;
        this.css = other.css;
        this.department = other.department;
        this.description = other.description;
        this.fadedTagColor = other.fadedTagColor;
        this.graphColor = other.graphColor;
        this.styleId = other.styleId;
        this.imagePath = other.imagePath;
        this.questionColor = other.questionColor;
        this.scrollBarColor = other.scrollBarColor;
        this.sentiment360Branding = other.sentiment360Branding;
        this.tagColor = other.tagColor;
        this.textColor = other.textColor;
        this.widgetSize = other.widgetSize;
    }
    
    public boolean isSentiment360Branding() {
            return sentiment360Branding;
    }

    public void setSentiment360Branding(boolean sentiment360Branding) {
            this.sentiment360Branding = sentiment360Branding;
    }

    public Department getDepartment() {
            return department;
    }
    public String getDescription() {
            return description;
    }

    public void setDepartment(Department department) {
            this.department = department;
    }
    
    public Integer getStyleId() {
        return styleId;
    }

    public void setStyleId(Integer styleId) {
        this.styleId = styleId;
    }

    public String getSize() {
        return widgetSize;
    }

    public void setSize(String size) {
        this.widgetSize = size;
    }
    
    public void setDescription(String desc) {
        this.description = desc;
    }

    public String getBackgroundImageType() {
        return backgroundImageType;
    }

    public void setBackgroundImageType(String backgroundImageType) {
        this.backgroundImageType = backgroundImageType;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getCss() {
        return css;
    }

    public void setCss(String css) {
        this.css = css;
    }

    public boolean isAnimated() {
        return animated;
    }

    public void setAnimated(boolean animated) {
        this.animated = animated;
    }

    public String getBrandingPath() {
        return brandingPath;
    }

    public void setBrandingPath(String brandingPath) {
        this.brandingPath = brandingPath;
    }

    public String getTagColor() {
        return tagColor;
    }

    public void setTagColor(String tagColor) {
        this.tagColor = tagColor;
    }

    public String getFadedTagColor() {
        return fadedTagColor;
    }

    public void setFadedTagColor(String fadedTagColor) {
        this.fadedTagColor = fadedTagColor;
    }

    public String getQuestionColor() {
        return questionColor;
    }

    public void setQuestionColor(String questionColor) {
        this.questionColor = questionColor;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public String getGraphColor() {
        return graphColor;
    }

    public void setGraphColor(String graphColor) {
        this.graphColor = graphColor;
    }

    public String getScrollBarColor() {
        return scrollBarColor;
    }

    public void setScrollBarColor(String scrollBarColor) {
        this.scrollBarColor = scrollBarColor;
    }

    public String getBackGroundColor() {
        return backGroundColor;
    }

    public void setBackGroundColor(String backGroundColor) {
        this.backGroundColor = backGroundColor;
    }

    @Override
    public String toString(){
        return "Style["+this.getStyleId()+"]";
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.styleId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Style other = (Style) obj;
        if (!Objects.equals(this.styleId, other.styleId)) {
            return false;
        }
        return true;
    }
}