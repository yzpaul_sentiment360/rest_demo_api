/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.sentiment360.pulse.dbclasses;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Sam
 */
@Entity
@Table(name = "feedbackMessage",
        indexes = {@Index(columnList = "categoryId"),
                   @Index(columnList = "priorityLevel")})
@XmlRootElement
public class FeedbackMessage implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @TableGenerator(name="FEEDBACKMESSAGE_GEN", table="SEQUENCE_TABLE", pkColumnName="SEQ_NAME",
            valueColumnName="SEQ_COUNT", pkColumnValue="FEEDBACKMESSAGE_SEQ")
    @GeneratedValue(strategy = GenerationType.TABLE, generator="FEEDBACKMESSAGE_GEN")
    @Column(name = "feedbackId")
    private Integer feedbackId;
    
    @Column(name = "ballotHeader1", length = 700)
    private String ballotHeader1;
    
    @Column(name = "ballotHeader2", length = 700)
    private String ballotHeader2;
    
    @Column(name = "ballotHeader3", length = 700)
    private String ballotHeader3;
    
    @Column(name = "ballotHeader4", length = 700)
    private String ballotHeader4;
    
    @Column(name = "priorityLevel")
    private Integer priorityLevel;
    
    @Column(name = "message", length = 700)
    private String message;
    
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "categoryId")
    private Category category;
    
    public FeedbackMessage() {}
    
    public FeedbackMessage(Category category, String ballotHeader1, String ballotHeader2, String ballotHeader3, 
            String ballotHeader4, Integer priorityLevel, String message) {
        this.category = category;
        this.ballotHeader1 = ballotHeader1;
        this.ballotHeader2 = ballotHeader2;
        this.ballotHeader3 = ballotHeader3;
        this.ballotHeader4 = ballotHeader4;
        this.priorityLevel = priorityLevel;
        this.message = message;
    }
    
    public Integer getFeedbackId() {
        return feedbackId;
    }
    
    public void setFeedbackId(Integer id) {
        this.feedbackId = id;
    }

    public Category getCategory() {
        return category;
    }
    
    public void setCategory(Category category) {
        this.category = category;
    }
    
    public String getBallotHeader1() {
        return ballotHeader1;
    }
    
    public void setBallotHeader1(String ballotHeader1) {
        this.ballotHeader1 = ballotHeader1;
    }
    
    public String getBallotHeader2() {
        return ballotHeader2;
    }
    
    public void setBallotHeader2(String ballotHeader2) {
        this.ballotHeader2 = ballotHeader2;
    }
    
    public String getBallotHeader3() {
        return ballotHeader3;
    }
    
    public void setBallotHeader3(String ballotHeader3) {
        this.ballotHeader3 = ballotHeader3;
    }
    
    public String getBallotHeader4() {
        return ballotHeader4;
    }
    
    public void setBallotHeader4(String ballotHeader4) {
        this.ballotHeader4 = ballotHeader4;
    }
    
    public Integer getPriorityLevel() {
        return priorityLevel;
    }
    
    public void setPriorityLevel(Integer priorityLevel) {
        this.priorityLevel = priorityLevel;
    }
    
    public String getMessage() {
        return message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (feedbackId != null ? feedbackId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FeedbackMessage)) {
            return false;
        }
        FeedbackMessage other = (FeedbackMessage) object;
        if ((this.feedbackId == null && other.feedbackId != null) || (this.feedbackId != null && !this.feedbackId.equals(other.feedbackId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "s360.core.FeedbackMessage[ id=" + feedbackId + " ]";
    }
    
}
