/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sentiment360.pulse.dbclasses;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jared
 */
@Entity
@Table(name = "story",
        indexes = {@Index(columnList = "categoryId"),
                   @Index(columnList = "type")})
@XmlRootElement
public class Story implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @TableGenerator(name="STORY_GEN", table="SEQUENCE_TABLE", pkColumnName="SEQ_NAME",
            valueColumnName="SEQ_COUNT", pkColumnValue="STORY_SEQ")
    @GeneratedValue(strategy = GenerationType.TABLE, generator="STORY_GEN")
    @Basic(optional = false)
    @Column(name = "storyId")
    private Integer storyId;
    
    @Column(name = "component")
    private String component;
    
    @Column(name = "type")
    private String type;
    
    @Column(name = "actionValue")
    private String actionValue;
    
    @Column(name = "content")
    private String content;
    
    @Column(name = "primaryLink")
    private String primaryLink;
    
    @Column(name = "itemId")
    private Integer itemId;
    
    @Column(name = "secondaryItemId")
    private Integer secondaryItemId;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dateRecorded")
    private Date dateRecorded;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    private Customer user;
    
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "categoryId")
    private Category category;
    
    public Story() {}
    
    public Integer getStoryId() {
        return storyId;
    }
    
    public void setStoryId(Integer id) {
        this.storyId = id;
    }
    
    public Category getCategory() {
        return category;
    }
    
    public void setCategory(Category category) {
        this.category = category;
    }
    
    public Customer getUser() {
        return user;
    }

    public void setUser(Customer user) {
        this.user = user;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getActionValue() {
        return actionValue;
    }

    public void setActionValue(String actionValue) {
        this.actionValue = actionValue;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPrimaryLink() {
        return primaryLink;
    }

    public void setPrimaryLink(String primaryLink) {
        this.primaryLink = primaryLink;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public Integer getSecondaryItemId() {
        return secondaryItemId;
    }

    public void setSecondaryItemId(Integer secondaryItemId) {
        this.secondaryItemId = secondaryItemId;
    }

    public Date getDateRecorded() {
        return dateRecorded;
    }

    public void setDateRecorded(Date dateRecorded) {
        this.dateRecorded = dateRecorded;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 13 * hash + Objects.hashCode(this.storyId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Story other = (Story) obj;
        if (!Objects.equals(this.storyId, other.storyId)) {
            return false;
        }
        return true;
    }
}
