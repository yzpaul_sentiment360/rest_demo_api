/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sentiment360.pulse.dbclasses;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jared
 */
@Entity
@Table(name = "category",
        indexes = {@Index(columnList = "departmentId"),
                   @Index(columnList = "userId"),
                   @Index(columnList = "deleted"),
                   @Index(columnList = "main")})
@XmlRootElement
public class Category implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @TableGenerator(name="CATEGORY_GEN", table="SEQUENCE_TABLE", pkColumnName="SEQ_NAME",
            valueColumnName="SEQ_COUNT", pkColumnValue="CATEGORY_SEQ")
    @GeneratedValue(strategy = GenerationType.TABLE, generator="CATEGORY_GEN")
    @Column(name= "categoryId")
    private Integer categoryId;
    
    @Column(name = "categoryName", length = 700)
    private String categoryName;
    
    @Column(name = "categoryLabel", length = 700)
    private String categoryLabel;
    
    @Column(name = "moderated")
    private boolean moderated;
    
    @Column(name = "powerGaugeEnabled")
    private boolean powerGaugeEnabled;
    
    @Column(name = "loginLevel")
    private Integer loginLevel;
    
    @Column(name = "main")
    private boolean main;
    
    @Column(name = "pgQuestion1", length = 700)
    private String pgQuestion1;
    
    @Column(name = "pgQuestion2", length = 700)
    private String pgQuestion2;
    
    @Column(name = "pgQuestion3", length = 700)
    private String pgQuestion3;
    
    @Column(name = "pgQuestion4", length = 700)
    private String pgQuestion4;
    
    @Column(name = "pgLabel1")
    private String pgLabel1;
    
    @Column(name = "pgLabel2")
    private String pgLabel2;
    
    @Column(name = "pgLabel3")
    private String pgLabel3;
    
    @Column(name = "pgLabel4")
    private String pgLabel4;
    
    @Column(name = "ballotHeaders1", length = 700)
    private String ballotHeaders1;
    
    @Column(name = "ballotHeaders2", length = 700)
    private String ballotHeaders2;
    
    @Column(name = "ballotHeaders3", length = 700)
    private String ballotHeaders3;
    
    @Column(name = "ballotHeaders4", length = 700)
    private String ballotHeaders4;
    
    @Column(name = "bannedUsers")
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "bannedUsers")
    private List<String> bannedUsers;
    
    @Column(name = "bannedIPs")
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "bannedIPs")
    private List<String> bannedIPs;
    
    @Column(name = "autoGenerateInsights")
    private boolean autoGenerateInsights;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "dateCreated")
    private Date dateCreated;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "latestCommentDate")
    private Date latestCommentDate;
    
    @Column(name = "deleted")
    private boolean deleted;
    
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "departmentId")
    private Department department;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    private Customer owner;
    
    public Category() {}

    public Category(Integer categoryId, String categoryName, boolean moderated, boolean powerGaugeEnabled, Integer loginLevel,
            boolean main, String pgQuestion1, String pgQuestion2, String pgQuestion3, String pgQuestion4, String ballotHeaders1, String ballotHeaders2,
            String ballotHeaders3, String ballotHeaders4, List<String> bannedUsers, List<String> bannedIPs, boolean autoGenerateInsights,
            Date dateCreated, Date latestCommentDate, boolean deleted, Department department, Customer owner) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.moderated = moderated;
        this.powerGaugeEnabled = powerGaugeEnabled;
        this.loginLevel = loginLevel;
        this.main = main;
        this.pgQuestion1 = pgQuestion1;
        this.pgQuestion2 = pgQuestion2;
        this.pgQuestion3 = pgQuestion3;
        this.pgQuestion4 = pgQuestion4;
        this.ballotHeaders1 = ballotHeaders1;
        this.ballotHeaders2 = ballotHeaders2;
        this.ballotHeaders3 = ballotHeaders3;
        this.ballotHeaders4 = ballotHeaders4;
        this.bannedUsers = bannedUsers;
        this.bannedIPs = bannedIPs;
        this.autoGenerateInsights = autoGenerateInsights;
        this.dateCreated = dateCreated;
        this.latestCommentDate = latestCommentDate;
        this.deleted = deleted;
        this.department = department;
        this.owner = owner;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
    
    public String getCategoryLabel() {
        return categoryLabel;
    }

    public void setCategoryLabel(String categoryLabel) {
        this.categoryLabel = categoryLabel;
    }

    public boolean isModerated() {
        return moderated;
    }

    public void setModerated(boolean moderated) {
        this.moderated = moderated;
    }

    public boolean isPowerGaugeEnabled() {
        return powerGaugeEnabled;
    }

    public void setPowerGaugeEnabled(boolean powerGaugeEnabled) {
        this.powerGaugeEnabled = powerGaugeEnabled;
    }

    public Integer getLoginLevel() {
        return loginLevel;
    }

    public void setLoginLevel(Integer loginLevel) {
        this.loginLevel = loginLevel;
    }

    public boolean isMain() {
        return main;
    }

    public void setMain(boolean main) {
        this.main = main;
    }

    public String getPgQuestion1() {
        return pgQuestion1;
    }

    public void setPgQuestion1(String pgQuestion1) {
        this.pgQuestion1 = pgQuestion1;
    }

    public String getPgQuestion2() {
        return pgQuestion2;
    }

    public void setPgQuestion2(String pgQuestion2) {
        this.pgQuestion2 = pgQuestion2;
    }

    public String getPgQuestion3() {
        return pgQuestion3;
    }

    public void setPgQuestion3(String pgQuestion3) {
        this.pgQuestion3 = pgQuestion3;
    }

    public String getPgQuestion4() {
        return pgQuestion4;
    }

    public void setPgQuestion4(String pgQuestion4) {
        this.pgQuestion4 = pgQuestion4;
    }
    
    public String getPgQuestion(int position)
    {
        switch (position) {
            case 1:
                return getPgQuestion1();
            case 2:
                return getPgQuestion2();
            case 3:
                return getPgQuestion3();
            case 4:
                return getPgQuestion4();
        }
        return "";
    }
    
    public void setPgQuestion(int position, String pgQuestion)
    {
        switch (position) {
            case 1:
                setPgQuestion1(pgQuestion);
                break;
            case 2:
                setPgQuestion2(pgQuestion);
                break;
            case 3:
                setPgQuestion3(pgQuestion);
                break;
            case 4:
                setPgQuestion4(pgQuestion);
                break;
        }
    }
    
    public String getPgLabel(int position)
    {
        switch (position) {
            case 1:
                return getPgLabel1();
            case 2:
                return getPgLabel2();
            case 3:
                return getPgLabel3();
            case 4:
                return getPgLabel4();
        }
        return "";
    }
    
    public void setPgLabel(int position, String pgLabel)
    {
        switch (position) {
            case 1:
                setPgLabel1(pgLabel);
                break;
            case 2:
                setPgLabel2(pgLabel);
                break;
            case 3:
                setPgLabel3(pgLabel);
                break;
            case 4:
                setPgLabel4(pgLabel);
                break;
        }
    }
    
    public String getPgLabel1() {
        return pgLabel1;
    }
    
    public void setPgLabel1(String pgLabel1) {
        this.pgLabel1 = pgLabel1;
    }
    
    public String getPgLabel2() {
        return pgLabel2;
    }
    
    public void setPgLabel2(String pgLabel2) {
        this.pgLabel2 = pgLabel2;
    }
    
    public String getPgLabel3() {
        return pgLabel3;
    }
    
    public void setPgLabel3(String pgLabel3) {
        this.pgLabel3 = pgLabel3;
    }
    
    public String getPgLabel4() {
        return pgLabel4;
    }
    
    public void setPgLabel4(String pgLabel4) {
        this.pgLabel4 = pgLabel4;
    }
    
    public String getBallotHeaders1() {
        return ballotHeaders1;
    }

    public void setBallotHeaders1(String ballotHeaders1) {
        this.ballotHeaders1 = ballotHeaders1;
    }

    public String getBallotHeaders2() {
        return ballotHeaders2;
    }

    public void setBallotHeaders2(String ballotHeaders2) {
        this.ballotHeaders2 = ballotHeaders2;
    }

    public String getBallotHeaders3() {
        return ballotHeaders3;
    }

    public void setBallotHeaders3(String ballotHeaders3) {
        this.ballotHeaders3 = ballotHeaders3;
    }

    public String getBallotHeaders4() {
        return ballotHeaders4;
    }

    public void setBallotHeaders4(String ballotHeaders4) {
        this.ballotHeaders4 = ballotHeaders4;
    }
    
    public String getBallotHeaders(int position) {
        switch (position) {
            case 1:
                return getBallotHeaders1();
            case 2:
                return getBallotHeaders2();
            case 3:
                return getBallotHeaders3();
            case 4:
                return getBallotHeaders4();
        }
        return "";
    }
    
    public void setBallotHeaders(int position, String ballotHeaders) {
        switch (position) {
            case 1:
                setBallotHeaders1(ballotHeaders);
                break;
            case 2:
                setBallotHeaders2(ballotHeaders);
                break;
            case 3:
                setBallotHeaders3(ballotHeaders);
                break;
            case 4:
                setBallotHeaders4(ballotHeaders);
                break;
        }
    }

    public List<String> getBannedUsers() {
        return bannedUsers;
    }

    public void setBannedUsers(List<String> bannedUsers) {
        this.bannedUsers = bannedUsers;
    }

    public List<String> getBannedIPs() {
        return bannedIPs;
    }

    public void setBannedIPs(List<String> bannedIPs) {
        this.bannedIPs = bannedIPs;
    }

    public boolean isAutoGenerateInsights() {
        return autoGenerateInsights;
    }

    public void setAutoGenerateInsights(boolean autoGenerateInsights) {
        this.autoGenerateInsights = autoGenerateInsights;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getLatestCommentDate() {
        return latestCommentDate;
    }

    public void setLatestCommentDate(Date latestCommentDate) {
        this.latestCommentDate = latestCommentDate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Customer getOwner() {
        return owner;
    }

    public void setOwner(Customer owner) {
        this.owner = owner;
    }
    
    public Integer getNumPolls() {
        int num = 0;
        for (int i = 1; i < 5; i++)
        {
            String header = getBallotHeaders(i);
            if (header == null || header.isEmpty())
                continue;
            num++;
        }
        
        return num;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.categoryId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Category other = (Category) obj;
        if (!Objects.equals(this.categoryId, other.categoryId)) {
            return false;
        }
        return true;
    }
}
