package com.sentiment360.quteedemorest;



import com.sentiment360.pulse.services.DepartmentService;
import com.sentiment360.pulse.services.HelloService;
import com.sentiment360.pulse.services.PermissionService;
import com.sentiment360.pulse.services.UserService;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * JAXActivator is an arbitrary name, what is important is that javax.ws.rs.core.Application is extended
 * and the @ApplicationPath annotation is used with a "rest" path.  Without this the rest routes linked to
 * from index.html would not be found.
 */
//@ApplicationPath("/rest")--- this is specified in web.xml
public class JAXActivator extends Application {
    
    public JAXActivator() {

    }
    
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<Class<?>>();
        classes.add(DepartmentService.class);
        classes.add(UserService.class);
        classes.add(PermissionService.class);
        classes.add(HelloService.class);
        return classes;
    }
}
